/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.extraPlugins = 'slideshow';
	//config.filebrowserUploadUrl = '/admin/main/upload';
	config.filebrowserBrowseUrl = 'http://lerad-wear.com/bundles/acmedemo/js/kcfinder/browse.php?opener=ckeditor&type=files';
	config.filebrowserImageBrowseUrl = 'http://lerad-wear.com/bundles/acmedemo/js/kcfinder/browse.php?opener=ckeditor&type=images';
	config.filebrowserFlashBrowseUrl = 'http://lerad-wear.com/bundles/acmedemo/js/kcfinder/browse.php?opener=ckeditor&type=flash';
	config.filebrowserUploadUrl = 'http://lerad-wear.com/bundles/acmedemo/js/kcfinder/upload.php?opener=ckeditor&type=files';
	config.filebrowserImageUploadUrl = 'http://lerad-wear.com/bundles/acmedemo/js/kcfinder/upload.php?opener=ckeditor&type=images';
	config.filebrowserFlashUploadUrl = 'http://lerad-wear.com/bundles/acmedemo/js/kcfinder/upload.php?opener=ckeditor&type=flash';
};
