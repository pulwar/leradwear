var markers = {};
var infowindowLevel = 0;
var infoWindow;
var map;
var mgr;

function loadCountries(country, city, countriesDropdown, citiesDropdown, isId) {
    //alert("loadCountries: " + countriesDropdown);
    $.getJSON("/StoreLocatorService.ashx", {
        method: "countries"
    },
    function (data) {
        //var countriesDropdown = $("#userCountry");
        $.each(data, function () {
            if (!isId && country != "" && this.Name.toString().toLowerCase() == country.toLowerCase()) {
                countriesDropdown.append($("<option />").val(this.Id).text(this.Name).attr('selected', true));
                loadCities(this.Id, city, citiesDropdown);
            }
            else {
                countriesDropdown.append($("<option />").val(this.Id).text(this.Name));
            }
        });
    
        if (isId) {
      $('option[value=' + country + ']', countriesDropdown).attr('selected', 'selected');
            loadCities(country, city, citiesDropdown);
        }
    
    });
}

function loadCities(countryId, city, citiesDropdown) {
    //alert("loadCities; countryId, city, citiesDropdown: " + countryId + ", " + city + " : " + citiesDropdown);
    $.getJSON("/StoreLocatorService.ashx", {
        method: "cities",
        countryId: countryId
    },
    function (data) {
        //var citiesDropdown = $("#userCity");
        citiesDropdown.html("");
        $.each(data, function () {
            if (isNaN(city) && city != null && this.Name.toString().toLowerCase() == city.toLowerCase()) {
                citiesDropdown.append($("<option />").val(this.Id).text(this.Name).attr('selected', true));
                loadLocations(this.Id);
            }
            else {
                citiesDropdown.append($("<option />").val(this.Id).text(this.Name));
                //alert(this.Name);
            }
        });
        
        if (!isNaN(city) && city != '') {
      $('option[value=' + city + ']', citiesDropdown).attr('selected', 'selected');
            loadLocations(city);
        }
    
        //citiesDropdown.addClass("refresh");
        //refreshCustomSelects();
    });
}

function loadLocations(cityId) {
    var str = "";
    var bounds = new google.maps.LatLngBounds();
    $.getJSON("/StoreLocatorService.ashx", {
        method: "stores",
        cityId: cityId
    },
    function (data) {
        $.each(data, function () {
            if (this.Latitude != "" && this.Longitude != "" && !isNaN(this.Latitude) && !isNaN(this.Longitude)) {
                str += "<li><a href='javascript:void(0)' onclick='centerMap(" + this.Id + ")'><span class='title'>" + this.Name + "</span><span class='info'>" + this.Address + ", " + this.Zipcode + "</span></a></li>";

                var point = new google.maps.LatLng(parseFloat(this.Latitude), parseFloat(this.Longitude));

                if (bounds.isEmpty()) {
                    bounds = new google.maps.LatLngBounds(point, point);
                }
                else {
                    bounds.extend(point);
                }

                var image = new google.maps.MarkerImage('/images/marker.png');
                var shadow = new google.maps.MarkerImage('/images/marker_shadow.png');

                var marker = new google.maps.Marker({
                    icon: image,
                    shadow: shadow,
                    position: point,
                    title: this.Name,
                    info: '<div style=\'height:80px;\'><p style=\'font-family:\"Arial\",Sans; overflow:hidden; font-size:12px; color:#024731;\'><strong>' + this.Name + '</strong><br />' + this.Address + ", " + this.Zipcode + '</p></div>'
                });

                mgr.addMarker(marker, 7);

                google.maps.event.addListener(marker, 'click', function () {
                    if (infoWindow) {
                        infoWindow.close();
                    }
                    infoWindow.setZIndex(++infowindowLevel);
                    infoWindow.setContent(marker.info);
                    infoWindow.open(map, marker);
                });
                marker.infoWindow = infoWindow;
                markers[this.Id] = marker;
            }
            else {
                str += "<li><a href='javascript:void(0)' onclick='searchAddress(\"" + this.Address + ", " + this.Zipcode + ", " + $('#userCity option:selected').text() + ", " + $('#userCountry option:selected').text() + "\")'><span class='title'>" + this.Name + "</span><span class='info'>" + this.Address + ", " + this.Zipcode + "</span></a></li>";
            }

        });
        if (!bounds.isEmpty()) map.fitBounds(bounds);
        if (data.length <= 1) map.setZoom(16);
        $("#storesListHolder").html(str);
        $("#storesLocatorList").show();
	    initStoresListScroll();
    });
}

function searchAddress(address) {
    //alert("searchAddress: " + address);
    var geocoder = new google.maps.Geocoder();
    var request = {};

    request.address = address;

    geocoder.geocode(request, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            map.fitBounds(results[0].geometry.viewport);
        }
    });
}

function centerMap(markerId) {
    var marker = markers[markerId];
    var point = marker.getPosition();
    map.setCenter(point);
    map.setZoom(16);
    if (infoWindow) {
        infoWindow.close();
    }
    infoWindow.setZIndex(++infowindowLevel);
    infoWindow.setContent(marker.info);
    infoWindow.open(map, marker);
}