// JavaScript Document

$(document).ready(function() {
  Page.init();
});

$(window)
.load(function() {
  Page.checkPageSize();
})
.resize(function() {
  Page.checkPageSize();
});

var Page = {
  subscribeSuccessfullMessage: '',
  subscribeErrorTitle: '',
  
  init: function() {
    Page.initMainNav();
    Page.initPageFooter();
    Page.checkPageSize();
    Page.customInputs();
    
    if ($('.general-content').length == 0) {
      $('.page-breadcrumbs').addClass('wide');
    }
  },
  
  checkPageSize: function() {
    var $c = $('.page-content').css({ 'height': 'auto' });
    var ch = $c.height();
    var wH = $(window).height();
    var delta = 95 + $('.page-breadcrumbs').height();
    if (wH - delta > ch) {
      $c.height(wH - delta);
    }
  },
  
  customInputs: function() {
    $('.custom-input:not(.inited)').each(function() {
      var $input = $(this);
      var $holder = $input.parent();
      var $watermark = $('.watermark', $holder);
      
      var watermark = $input.data('watermark') || '';
      
      $holder.click(function() {
        $input.focus();
      });
      
      if ($input.attr('type').toLowerCase() == 'password') {
        $holder.css({ 'position': 'relative' });
        $watermark.text(watermark);
        if ($.trim($input.val()) == '') {
          $watermark.show();
        } else {
          $watermark.hide();
        }
        
        $watermark.click(function() {
          $input.focus();
        });
        
        $input
        .focus(function() {
          $watermark.hide();
          $holder.addClass('focus').removeClass('invalid');
        })
        .blur(function() {
          $holder.removeClass('focus');
          if ($input.val() == '') {
            $input.val('');
            $watermark.show();
          }
        });
      } else {
        if ($.trim($input.val()) == watermark || $.trim($input.val()) == '') {
          $input.val(watermark).addClass('watermark');
        } else {
          $input.removeClass('watermark');
        }
        
        $input
        .focus(function() {
          $holder.addClass('focus').removeClass('invalid');
          $input.removeClass('watermark');
          if ($.trim($input.val()) == watermark) {
            $input.val('');
          }
        })
        .blur(function() {
          $holder.removeClass('focus');
          if ($.trim($input.val()) == watermark || $.trim($input.val()) == '') {
            $input.val(watermark).addClass('watermark');
          }
        });
      }
    });
    
    $('.custom-textarea:not(.inited)').each(function() {
      var $input = $(this);
      var $holder = $input.parent();
      var $watermark = $('.watermark', $holder);
      
      var watermark = $input.data('watermark') || '';
      
      $holder.click(function() {
        $input.focus();
      });
      
      if ($.trim($input.val()) == watermark || $.trim($input.val()) == '') {
        $input.val(watermark).addClass('watermark');
      } else {
        $input.removeClass('watermark');
      }
      
      $input
      .focus(function() {
        $holder.addClass('focus').removeClass('invalid');
        $input.removeClass('watermark');
        if ($.trim($input.val()) == watermark) {
          $input.val('');
        }
      })
      .blur(function() {
        $holder.removeClass('focus');
        if ($.trim($input.val()) == watermark || $.trim($input.val()) == '') {
          $input.val(watermark).addClass('watermark');
        }
      });
    });
  },
  
  initMainNav: function() {
    var $nav = $('.page-header .main-nav > ul');
    $('li.active', $nav).parents('li').addClass('active');
    var $active = $('> li.active ul', $nav);
    
    $('li ul', $nav).each(function() {
      var $parent = $(this);
      var $pointer = $parent.append('<li class="pointer"><!--  --></li>').find('.pointer').css({ 'margin-left': 10 + ($parent.parent().width() - 13) / 2 });
      var _parentLeft = Math.floor($parent.parent().position().left);
      var _shift = $nav.width() - $parent.width() - _parentLeft - 10;
      if (_shift < 0) {
        $parent.css({ 'left': _shift });
        $pointer.css({ 'left': - _shift });
      }
    });
    
    $('> li:not(.active)', $nav).hover(
      function() {
        var $sub = $('ul', this);
        if ($sub.length){
          $sub.fadeIn(100);
          $active.fadeOut(50);
        }
      },
      function() {
        var $sub = $('ul', this);
        if ($sub.length){
          $sub.stop(true, true).fadeOut(50);
          $active.stop(true, true).fadeIn(100);
        }
      }
    );
  },
  
  initPageFooter: function() {
    $('#selectLanguage1, #selectLanguage2').selectik();
    
    var $footer = $('.page-footer');
    
    var $set = $('.collapsed .level-item, .collapsed .ready-hover');
    /*
    $set.mouseenter(function() {
      $('.expanded', $footer).stop(true, true).slideDown(200);
      $('.collapsed', $footer).stop(true, true).slideUp(200);
    });
    
    $footer.mouseleave(function() {
      $('.expanded', $footer).stop(true, true).slideUp(200);
      $('.collapsed', $footer).stop(true, true).slideDown(200);
    });
    */
  },
  
  initHomeCarousel: function(_time) {
    var $carousel = $('#homeCarousel');
    var $slides = $('.slides li', $carousel);
    var slidesNumber = $slides.length;
    var currentSlide = 0;
    var switchTime = _time || 5000;
    var $btnNext = $('.btn-next', $carousel);
    var $btnPrev = $('.btn-prev', $carousel);
    var isSwitching = false;
    var _timer;
    
    $slides.each(function() {
      var $img = $('img', this);
      $img
      .attr('src', $img.data('src'))
      .one('load', function() {
        arrangeImage($img);
      });
    });
    
    $btnNext.click(function() {
      nextSlide();
    });
    
    $btnPrev.click(function() {
      nextSlide(-1);
    });
    
    $(window).resize(function() {
      $slides.each(function() {
        arrangeImage($('img', this));
      });
    });
    
    _timer = setTimeout(nextSlide, switchTime);
    
    function nextSlide(_dir) {
      if (isSwitching) { return; }
      isSwitching = true;
      clearTimeout(_timer);
      var dir = _dir || 1;
      var $slide = $('li:eq(' + currentSlide + ')', $carousel);
      var $content = $('.content', $slide);
      $slide.animate({ 'left': - dir * 15, 'opacity': 0 }, { duration: 600, complete: function() { $(this).hide(); } });
      if ($content.hasClass('right')) {
        $content.animate({ 'margin-left': - 135 - dir * 15, 'opacity': 0 }, 800);
      } else {
        $content.animate({ 'margin-left': - 511 - dir * 15, 'opacity': 0 }, 800);
      }
      currentSlide = currentSlide + dir;
      if (currentSlide == slidesNumber) { currentSlide = 0; }
      if (currentSlide == -1) { currentSlide = slidesNumber - 1; }
      var $slide = $('li:eq(' + currentSlide + ')', $carousel);
      var $content = $('.content', $slide);
      $slide.css({ 'left': dir * 15, 'opacity': 0 }).show().animate({ 'left': 0, 'opacity': 1 }, 600);
      if ($content.hasClass('right')) {
        $content.css({ 'margin-left': - 135 + dir * 15, 'opacity': 0 }).animate({ 'margin-left': - 135, 'opacity': 1 }, 800);
      } else {
        $content.css({ 'margin-left': - 511 + dir * 15, 'opacity': 0 }).animate({ 'margin-left': - 511, 'opacity': 1 }, 800);
      }
      arrangeImage($('img', $slide));
      setTimeout(function() { 
        isSwitching = false;
        _timer = setTimeout(nextSlide, switchTime);
       }, 800);
    }
    
    function arrangeImage(_img) {
      var W = $carousel.width();
      var H = $carousel.height();
      _img.css({ 'width': '100%', 'height': 'auto' });
      if (_img.height() < H) {
        _img.css({ 'height': H, 'width': 'auto' });
      }
      _img.css({ 'margin-left': (W - _img.width()) / 2 });
    }
  },
  
  changeLanguage: function() {
    
  },
  
  subscribeNews: function(input) {
    var _email = $.trim($(input).val());
    if (_email == $(input).data('watermark') || _email == '') {
      $(input).parent().addClass('invalid');
    } else {
      $.getJSON("SubscribeToNewsletter.ashx", {
        email: _email
      },
      function (data) {
        if(data.Success == 'True'){
          alert(Page.subscribeSuccessfullMessage);
        }
        else{
          alert(Page.subscribeErrorTitle + ': ' + data.Success[0].Error);
        }
      });
    }
    
  },
  
  initHistory: function() {
    var $blocksHolder = $('#historyYearsHolder');
    var $pathDark = $('#historyPathDark');
    var $pathLight = $('#historyPathLight');
    var $pointsHolder = $('#pointsHolder');
    
    var currentPathPos = 0;
    var maxScroll = $(document).height() - $(window).height();
    var pathHeight = 1636;
    var delta = pathHeight / maxScroll;
    
    var pointsPos = new Array(1, 227, 460, 692, 925, 1159, 1392, 1625);
    var pointsLength = pointsPos.length;
    
    $('div.item', $blocksHolder).each(function(i) {
      $('h2', this).after('<h2 class="hover">' + $('h2', this).text() + '</h2>');
      var $year = $('h2.hover', this);
      var $image0 = $('.image-holder img:eq(0)', this);
      var $image1 = $('.image-holder img:eq(1)', this);
      $image0.css('opacity', 1);
    
      $(this).hover(
        function() {
          if (!$year.hasClass('light')) { $year.fadeIn(300); }
          $image0.stop(true, false).delay(100).animate({opacity: 0}, 200);
          $image1.stop(true, false).animate({opacity: 1}, 200);
        },
        function() {
          if (!$year.hasClass('light')) { $year.stop(true, true).fadeOut(300); }
          $image0.stop(true, false).animate({opacity: 1}, 100);
          $image1.stop(true, false).animate({opacity: 0}, 200);
        }
      );
    });
    
    setPath();
    
    $(window)
    .scroll(function() {
      setPath();
    })
    .resize(function() {
      maxScroll = $(document).height() - document.body.scrollHeight;
      delta = pathHeight / maxScroll;
    });
    
    function setPath() {
      var currentPathPos = Math.floor(delta * $(window).scrollTop());
      $pathLight.stop(true, false).animate({ height: currentPathPos }, 800);
      $pathDark.stop(true, false).animate({ height: pathHeight - currentPathPos }, 800);
      for (var i = 0; i < pointsLength; i++) {
        var $point = $('div:eq(' + i + ')', $pointsHolder);
        var $year = $('div.item:eq(' + i + ') h2.hover', $blocksHolder);
        var $subtitle = $('div.item:eq(' + i + ') h3', $blocksHolder);
        if (currentPathPos > pointsPos[i]) {
          if (!$point.hasClass('light')) {
            $point.fadeIn(500).addClass('light');
            $year.fadeIn(300).addClass('light');
            $subtitle.addClass('light');
          }
        } else {
          if ($point.hasClass('light')) {
            $point.fadeOut(500).removeClass('light');
            $year.fadeOut(300).removeClass('light');
            $subtitle.removeClass('light');
          }
        }
      }
    }
  },
  
  initLookbook: function() {
    var $lookbook = $('#lookbookHolder');
    var $list = $('#lookbookList > ul');
    var $slides = $('> li', $list);
    var $preview = $('#lookbookPreview');
    var $btnClose = $('.btn-close', $preview);
    var $slider = $('#lookbookSlider');
    var $products = $('#lookbookProducts');
    var $btnNext = $('.btn-next', $lookbook);
    var $btnPrev = $('.btn-prev', $lookbook);
    var lb = {
      mode: 'list',
      listWidth: 0,
      currentSlide: 0,
      loadSlide: 0,
      slidesNumber: $slides.length,
      slides: new Array($slides.length),
      sliderWidth: 0
    };
    var resizeTimer;
    var isAnimated = false;
    
    $(document).ready(function() {
      resetSize();
      $(window)
      .resize(function() {
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(resetSize, 500);
      })
      .bind('keydown', function(e) {
        switch(e.which) {
          case 37: // Arrow left
            if (lb.mode == 'list') {
              scrollScreen(1);
            } else {
              scrollSlide(1);
            }
            break;
            
          case 38: // Arrow up
            
            break;
            
          case 39: // Arrow right
            if (lb.mode == 'list') {
              scrollScreen(-1);
            } else {
              scrollSlide(-1);
            }
            break;
            
          case 40: // Arrow down
            
            break;
        }
      })
      
      loadList();
      
      $btnNext.click(function() {
        if (lb.mode == 'list') {
          scrollScreen(-1);
        } else {
          scrollSlide(-1);
        }
      });
      
      $btnPrev.click(function() {
        if (lb.mode == 'list') {
          scrollScreen(1);
        } else {
          scrollSlide(1);
        }
      });
      
      $btnClose.click(function() {
        closePreview();
      });
    });
    
    function scrollScreen(_dir) {
      if (isAnimated) { return; }
      isAnimated = true;
      var _x = Math.floor($list.position().left);
      _x = _x + _dir * lb.width;
      if (_x < lb.width - lb.listWidth) {
        _x = lb.width - lb.listWidth;
      }
      if (_x > 0) {
        _x = 0;
      }
      $list.animate({ 'left': _x }, { duration: 1000, easing: 'easeInOutCubic', complete: function() { isAnimated = false; } });
      
      var _sx = _x * (lb.width - lb.sliderWidth) / (lb.width - lb.listWidth);
      $('a', $slider).animate({ 'left': _sx }, 1000);
    }
    
    function loadList() {
      var $slide = $('> li:eq(' + lb.loadSlide + ')', $list).data('index', lb.loadSlide);
      var $dataspan = $('span', $slide);
      $slide.append('<img src="' + $dataspan.data('src') + '" alt="" data-big="' + $dataspan.data('big') + '" />');
      $dataspan.remove();
      var $img = $('img', $slide);
      var $pointer = $('ins', $slide);
      
      $slide
        .hover(
          function() {
            $pointer.fadeIn(200);
          },
          function() {
            $pointer.stop(true, true).fadeOut(100);
          }
        )
        //.find('ins')
        .click(function() {
          openPreview($slide.data('index'));
        });
      
      $img
        .one('load', function() {
          $img.css({ 'width': 'auto', 'height': lb.height }).fadeIn(200, function() {
            var _w = $img.width();
            $img.css({ 'display': 'block' }).parent().width(_w);
            setSlider(_w);
            lb.slides[lb.loadSlide] = _w;
            lb.loadSlide++;
            if (lb.loadSlide < lb.slidesNumber) {
              loadList();
            }
          });
        })
        .attr('src', $img.data('src'));
    }
    
    function openPreview(i) {
      lb.mode = 'preview';
      lb.currentSlide = i;
      var $source = $('> li:eq(' + lb.currentSlide + ')', $list);
      var $data = $('.products', $source);
      
      var _sx = $source.offset().left;
      var _sy = 0;

      var $prodList = $('.products', $products).empty();
      $('li', $data).each(function() {
        var $p = $($(this).html());
        $p.append('<span>' + $(this).data('price') + '</span>');
        $prodList.append($p);
      });
      
      $('img', $source).clone().appendTo($preview);
      $preview.show();
      var $img = $('img:last-child', $preview);
      $img
      .css({ 'left': _sx, 'top': _sy, 'width': $img.width(), 'height': 'auto' })
      .animate({ 
        'width': lb.width, 
        'left': 0 
        }, { 
        duration: 1000, 
        easing: 'easeInOutQuint', 
        complete: function() {
          //$btnClose.fadeIn(200);
          loadBigImage($img, lb.currentSlide);
        }
      });
      
      $products.animate({ 'bottom': 0 }, { duration: 300 });
    }
    
    function closePreview() {
      lb.mode = 'list';
      
      $btnClose.fadeOut(100);
      
      var $source = $('> li:eq(' + lb.currentSlide + ')', $list);
      var _lx = (lb.width - $source.width()) / 2 - $source.position().left;
      if (_lx > 0) { _lx = 0; }
      if (_lx < lb.width - lb.listWidth) { _lx = lb.width - lb.listWidth; }
      $list.css({ 'left': _lx });
      
      $('a', $slider).css({ 'left': _lx * (lb.width - lb.sliderWidth) / (lb.width - lb.listWidth) });
      
      var _sx = $source.offset().left;
      
      
      $('img', $preview).draggable('destroy');
      $('img:not(:last-child)', $preview).remove();
      $('img', $preview)
      .stop(true, true)
      .css({ 'width': 'auto' })
      .animate({ 'top': 0, 'left': _sx, 'height': lb.height }, { 
        duration: 600, 
        easing: 'easeInOutCubic', 
        complete: function() {
          $(this).remove();
          $preview.hide();
        }  
      });
      
      $products.animate({ 'bottom': -80 }, { duration: 300 });
    }
    
    function loadBigImage($img) {
      var iW = $img.width();
      var iH = $img.height();
      var iT = parseInt($img.css('top'));
      var iL = parseInt($img.css('left'));
      var $big = $('<img />', { 'alt': '', 'src': $img.data('big') });
      $big.css({ 'display': 'none', 'width': iW, 'height': iH, 'top': iT, 'left': iL });
      $big.one('load', function() {
        $big.fadeIn(300, function() {
          isAnimated = false;
          $img.remove();
          var startTime, startY;
          $big.draggable({ 
            axis: 'y', 
            containment: [0, 40 - iH + lb.height, 0, 105],
            start: function(e, ui) {
              ui.helper.stop();
              var d = new Date();
              startTime = d.getTime();
              startY = ui.position.top;
            },
            stop: function(e, ui) {
              var d = new Date();
              var curY = ui.position.top;
              var finalY = ui.position.top - 100 * (startY - ui.position.top) / (d.getTime() - startTime);
              if (finalY > 0) { finalY = 0; }
              if (finalY < lb.height - 65 - iH) { finalY = lb.height - 65 - iH; }
              ui.helper.animate({ 'top': finalY }, { duration: 400, easing: 'easeOutCubic' });
            }
          });
          $btnClose.fadeIn(200);
        });
      });
      
      $preview.append($big);
      
      var startTime, startY;
      
      $img.draggable({ 
        axis: 'y', 
        containment: [0, 40 - iH + lb.height, 0, 105],
        start: function(e, ui) {
          ui.helper.stop();
          var d = new Date();
          startTime = d.getTime();
          startY = ui.position.top;
        },
        stop: function(e, ui) {
          var d = new Date();
          var curY = ui.position.top;
          var finalY = ui.position.top - 100 * (startY - ui.position.top) / (d.getTime() - startTime);
          if (finalY > 0) { finalY = 0; }
          if (finalY < lb.height - 65 - iH) { finalY = lb.height - 65 - iH; }
          ui.helper.animate({ 'top': finalY }, { duration: 400, easing: 'easeOutCubic' });
          $big.animate({ 'top': finalY }, { duration: 400, easing: 'easeOutCubic' });
        },
        drag: function(event, ui) {
          $big.css({ 'top': ui.position.top });
        }
      });
    }
    
    function scrollSlide(dir) {
      if (isAnimated) { return; }
      isAnimated = true;
      
      lb.currentSlide -= dir;
      if (lb.currentSlide < 0) { lb.currentSlide = lb.slidesNumber - 1; }
      if (lb.currentSlide >= lb.slidesNumber) { lb.currentSlide = 0; }
      
      var $source = $('> li:eq(' + lb.currentSlide + ')', $list);
      var $data = $('.products', $source);
      
      var $prodList = $('.products', $products).empty();
      $('li', $data).each(function() {
        var $p = $($(this).html());
        $p.append('<span>' + $(this).data('price') + '</span>');
        $prodList.append($p);
      });
      
      var $img = $('img', $source).clone();
      $img
      .css({ 'top': 0, 'left': - 20 * dir, 'width': lb.width, 'height': 'auto', 'opacity': 0 })
      .appendTo($preview)
      .animate({ 'left': 0, 'opacity': 1 }, {
        duration: 400,
        complete: function () {
          $('img:not(:last-child)', $preview).remove();
          loadBigImage($img);
        }
      })
    }
    
    function setSlider(_width) {
      lb.listWidth += _width;
      $list.width(lb.listWidth);
      lb.sliderWidth = Math.floor(lb.width * lb.width / lb.listWidth);
      if (lb.sliderWidth > lb.width) { lb.sliderWidth = lb.width; }
      
      var $bar = $('a', $slider);
      if ($bar.hasClass('ui-draggable')) {
        $bar.draggable('destroy');
      }
      $bar
        .css({ 'width': lb.sliderWidth })
        .draggable({
          axis: 'x',
          containment: 'parent',
          drag: function(e, ui) {
            var listX = ui.position.left * (lb.width - lb.listWidth) / (lb.width - lb.sliderWidth);
            $list.css({ 'left': listX });
          }
        });
    }
    
    function resetSize() {
      lb.width = $lookbook.width();
      lb.height = $lookbook.height() - 10;
      
      lb.listWidth = 0;
      $slides.each(function() {
        lb.listWidth += $(this).width();
      });
      setSlider(0);
      
      $preview.height(lb.height);
      $list.parent().height(lb.height);
      
    }
  },
  
  initShops: function() {
    
  },
  
  initPressClipping: function() {
    var $list = $('.press-clippings-list');
    var itemsNumber = $('li', $list).length;
    var rows = Math.ceil(itemsNumber / 3);
    for (var r = 0; r < rows; r++) {
      var h = 0;
      for (var i = 0; i < 3; i++) {
        if (r * 3 + i < itemsNumber) {
          var $li = $('li:eq(' + (r * 3 + i) + ')', $list);
          h = Math.max(h, $li.height());
        }
      }
      for (var i = 0; i < 3; i++) {
        if (r * 3 + i < itemsNumber) {
          var $li = $('li:eq(' + (r * 3 + i) + ')', $list);
          $li.height(h);
          var $img = $('img', $li);
          $img.one('load', function() {
            $img.css({ 'margin-left': ($li.width() - $img.width()) /2 });
          });
        }
      }
    }
  },
  
  submitContact: function(id) {
      if(Page.validateContactForm()){
          $.getJSON("DoContact.ashx", {
              name: $('#userName').val(),
              phone: $('#userPhone').val(),
              email: $('#userEmail').val(),
              comments: $('#userComments').val(),
              id: id
          },
          function (data) {
            Page.checkMessageSent(data);
          });
        }
    },
  
  validateContactForm: function() {
    $('.contact-form .sent-text').hide();
    var isFormOK = true;
    $('.contact-form input.required, .contact-form textarea.required').each(function() {
      if ($.trim($(this).val()) == '' || $.trim($(this).val()) == $(this).data('watermark')) {
        isFormOK = false;
        $(this).parent().addClass('invalid');
      }
    });
    if (!isFormOK) {
      $('.contact-form .required-text').fadeIn(200);
    }
    return isFormOK;
  },
  
  checkMessageSent: function(jdata) {
    if (jdata.Success === 'True') {
      $('.contact-form input:not(.btn-submit), .contact-form textarea').each(function() {
        $(this).addClass('inactive').val($(this).data('watermark') + ' *').parent().removeClass('focus invalid');
      });
      $('.contact-form .sent-text').fadeIn(200);
    } else {
      var formOK = true;
      for (var i = 0; i < jdata.Success.length; i++) {
        switch(jdata.Success[i].Error) {
          case 'Invalid email' :
          $('#userEmail').parent().addClass('invalid');
          formOK = false;
          break;
        }
      }
      if (!formOK) {
        $('.contact-form .required-text').fadeIn(200);
      }
    }
  } 

}

var Stores = {
  storesList: [],
  map: {},
  mgr: {},
  infoWindow: {},
  markers: [],
  //mcOptions: {styles: [{ textColor: '#FFFFFF', width: 44, height: 98, anchor: [18, 0], url: "/images/map-claster.png" }]},
  mcOptions: {styles: [{ textColor: '#FFFFFF', width: 44, height: 44, url: "/images/map-claster.png" }]},
  markerCluster: {},
  
  initMaps: function() {
    $("#draggableBlock").draggable({ handle: "#draggableHelper" });
    
    var prevQuery = '';
    $('#userStoreLocation')
    .val('')
	.blur()
    .keyup(function(e) {
      var query = $.trim($(this).val());
      //if (query.length < 3) { return; }
      if (prevQuery != query) {
        var _data = {
          method: 'find',
          input: query
        }
      $.ajax({
          url: 'StoreLocatorService.ashx',
          data: _data,
          success: function(data) {
            Stores.storesList = data.Stores;
			if (query.length > 0) {
	            buildList(Stores.storesList);
			} else {
				buildList([]);
				Stores.setLocation();
			}
          }
        });
        prevQuery = query;
      }
    });
	
    function buildList(_data) {
      var itemsNumber = _data.length;
      $('#storesList').empty();
      
      for (var s = 0; s < itemsNumber; s++) {
        $('#storesList').append('<li><a onclick="Stores.centerMap(' + _data[s].Id + ')"><span class="title">' + _data[s].Name + '</span><span class="address">' + _data[s].City + ', ' + _data[s].Address + ', ' + _data[s].Zipcode + '</span><ins><!--  --></ins></a></li>');
      }
      
      if (itemsNumber > 0) {
        Stores.setLocation();
        $('#storesLocatorList').show();
      } else {
        $('#storesLocatorList').hide();
      }
    }

    var GarciaStyle = [{ stylers: [{ hue: "#584733" }, { saturation: -90}]}];

    GarciaStyle = [{
        stylers: [
          { hue: "#00ffe6" },
          { saturation: -100 }
        ]
      },{
        featureType: "road",
        elementType: "geometry",
        stylers: [
          { lightness: 100 },
          { visibility: "simplified" }
        ]
      },{
        featureType: "road",
        elementType: "labels",
        stylers: [
          { visibility: "off" }
        ]
      }];
    
    
    var myOptions = {
      zoom: 5,
      zoomControlOptions: {
        style: google.maps.ZoomControlStyle.SMALL
      },
      mapTypeControl: false,
      panControl: false,
	  //streetViewControl: false,
      center: StoresSettings.latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    
    Stores.map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    Stores.map.setOptions({ styles: GarciaStyle });
    Stores.mgr = new MarkerManager(Stores.map, { trackMarkers: true, maxZoom: 15 });
    Stores.infoWindow = new google.maps.InfoWindow();
	
	$.ajax({
		url: 'StoreLocatorService.ashx',
		data: {
			method: 'find',
			input: ''
		},
		success: function(data) {
			//console.log(data);
			Stores.storesList = data.Stores;
			Stores.setLocation();
		}
	});
  },
  
  setLocation: function() {
    var bounds = new google.maps.LatLngBounds();
	Stores.markers = [];
	Stores.mgr.clearMarkers();
	if (Stores.markerCluster.clearMarkers) {
		Stores.markerCluster.clearMarkers();
	}
	var clMarkers = [];
    
    $.each(Stores.storesList, function () {
            if (this.Latitude != "" && this.Longitude != "" && !isNaN(this.Latitude) && !isNaN(this.Longitude)) {
                var point = new google.maps.LatLng(parseFloat(this.Latitude), parseFloat(this.Longitude));
                if (bounds.isEmpty()) {
                    bounds = new google.maps.LatLngBounds(point, point);
                }
                else {
                    bounds.extend(point);
                }

                var image = new google.maps.MarkerImage('/images/marker.png');

                var marker = new google.maps.Marker({
                    icon: image,
                    //shadow: shadow,
                    position: point,
                    title: this.Name,
                    info: '<div style=\'height:80px;\'><p style=\'font-family:\"Arial\",Sans; overflow:hidden; font-size:12px; color:#024731;\'><strong>' + this.Name + '</strong><br />' + this.City + ', ' + this.Address + ', ' + this.Zipcode + '</p></div>'
                });

                Stores.mgr.addMarker(marker, 7);

                google.maps.event.addListener(marker, 'click', function () {
                    if (Stores.infoWindow) {
                        Stores.infoWindow.close();
                    }
                    Stores.infoWindow.setZIndex(++infowindowLevel);
                    Stores.infoWindow.setContent(marker.info);
                    Stores.infoWindow.open(Stores.map, marker);
                });
                marker.infoWindow = infoWindow;
                Stores.markers[this.Id] = marker;
				clMarkers.push(marker);
            }
        });
    
    if (!bounds.isEmpty()) Stores.map.fitBounds(bounds);
    if (Stores.storesList.length <= 1) Stores.map.setZoom(16);
	
	Stores.markerCluster = new MarkerClusterer(Stores.map, clMarkers, Stores.mcOptions);
  },
  
  centerMap: function(markerId) {
    var marker = Stores.markers[markerId];
    var point = marker.getPosition();
    Stores.map.setCenter(point);
    Stores.map.setZoom(16);
    if (Stores.infoWindow) {
      Stores.infoWindow.close();
    }
    Stores.infoWindow.setZIndex(++infowindowLevel);
    Stores.infoWindow.setContent(marker.info);
    Stores.infoWindow.open(Stores.map, marker);
  }
}

