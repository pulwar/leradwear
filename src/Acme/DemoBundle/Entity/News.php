<?php

namespace Acme\DemoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Acme\DemoBundle\Entity\News
 * 
 * @ORM\Table(name="moda_news")
 * @ORM\Entity(repositoryClass="Acme\DemoBundle\Repository\MainRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class News 
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="ID", type="integer",length=3)
     * @ORM\id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name     
     * @ORM\Column(name="`name`", type="string", length=255, nullable=TRUE)     
     */
    private $name; 

    /**
     * @var Datetime $Stamp
     *
     * @ORM\Column(name="Stamp", type="datetime", nullable=FALSE)
     *
     */
    private $Stamp;
   
    /**
     * @var text
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;  

    /**
     * @var integer $hide
     * @ORM\Column(name="`hide`", type="integer", length=3, nullable=TRUE)
     */
    private $hide;

    /**
     * @var integer $order
     * @ORM\Column(name="`order`", type="integer", length=3, nullable=TRUE)
     */
    private $order; 

     /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->setStamp(new \DateTime()); 
    }   

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return News
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set Stamp
     *
     * @param \DateTime $stamp
     * @return News
     */
    public function setStamp($stamp)
    {
        $this->Stamp = $stamp;

        return $this;
    }

    /**
     * Get Stamp
     *
     * @return \DateTime 
     */
    public function getStamp()
    {
        return $this->Stamp;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return News
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set hide
     *
     * @param integer $hide
     * @return News
     */
    public function setHide($hide)
    {
        $this->hide = $hide;

        return $this;
    }

    /**
     * Get hide
     *
     * @return integer 
     */
    public function getHide()
    {
        return $this->hide;
    }

    /**
     * Set order
     *
     * @param integer $order
     * @return News
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer 
     */
    public function getOrder()
    {
        return $this->order;
    }
}
