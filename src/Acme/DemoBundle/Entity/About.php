<?php

namespace Acme\DemoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Acme\DemoBundle\Entity\Client
 * 
 * @ORM\Table(name="moda_about")
 * @ORM\Entity(repositoryClass="Acme\DemoBundle\Repository\MainRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class About 
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="ID", type="integer",length=3)
     * @ORM\id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name     
     * @ORM\Column(name="`name`", type="string", length=255, nullable=FALSE)     
     */
    private $name; 

    /**
     * @var string $image
    * @ORM\Column(name="`image`", type="string", length=255, nullable=TRUE)   
     */
    private $image; 

    /**
     * @var string $type     
     * @ORM\Column(name="`type`", type="string", length=255, nullable=FALSE)     
     */
    private $type; 

    /**
     * @var text $description
     * @ORM\Column(name="description", type="text", nullable=FALSE)
     */
    public $description; 

    /**
     * @var integer $hide
     * @ORM\Column(name="`hide`", type="integer", length=3, nullable=TRUE)
     */
    private $hide;

    /**
     * @var integer $order
     * @ORM\Column(name="`order`", type="integer", length=3, nullable=TRUE)
     */
    private $order;   



    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return About
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return About
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return About
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return About
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set hide
     *
     * @param integer $hide
     * @return About
     */
    public function setHide($hide)
    {
        $this->hide = $hide;

        return $this;
    }

    /**
     * Get hide
     *
     * @return integer 
     */
    public function getHide()
    {
        return $this->hide;
    }

    /**
     * Set order
     *
     * @param integer $order
     * @return About
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer 
     */
    public function getOrder()
    {
        return $this->order;
    }
}
