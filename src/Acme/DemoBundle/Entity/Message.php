<?php

namespace Acme\DemoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Acme\DemoBundle\Entity\Message
 * 
 * @ORM\Table(name="moda_message") 
* @ORM\Entity(repositoryClass="Acme\DemoBundle\Repository\MainRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Message 
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="ID", type="integer",length=3)
     * @ORM\id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name     
     * @ORM\Column(name="`name`", type="string", length=255, nullable=TRUE)     
     */
    private $name;

     /**
     * @var string $mail     
     * @ORM\Column(name="mail", type="string", length=255, nullable=TRUE)     
     */
    private $mail; 

     /**
     * @var string $phone     
     * @ORM\Column(name="phone", type="string", length=255, nullable=TRUE)     
     */
    private $phone;

    /**
     * @var Datetime $Stamp
     *
     * @ORM\Column(name="Stamp", type="datetime", nullable=FALSE)
     *
     */
    private $Stamp;
   
    /**
     * @var text
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;  

    /**
     * @var integer $hide
     * @ORM\Column(name="`hide`", type="integer", length=3, nullable=TRUE)
     */
    private $hide;

    /**
     * @var integer $order
     * @ORM\Column(name="`order`", type="integer", length=3, nullable=TRUE)
     */
    private $order; 

     /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->setStamp(new \DateTime()); 
    }   

   

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Message
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set mail
     *
     * @param string $mail
     * @return Message
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string 
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Message
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set Stamp
     *
     * @param \DateTime $stamp
     * @return Message
     */
    public function setStamp($stamp)
    {
        $this->Stamp = $stamp;

        return $this;
    }

    /**
     * Get Stamp
     *
     * @return \DateTime 
     */
    public function getStamp()
    {
        return $this->Stamp;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Message
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set hide
     *
     * @param integer $hide
     * @return Message
     */
    public function setHide($hide)
    {
        $this->hide = $hide;

        return $this;
    }

    /**
     * Get hide
     *
     * @return integer 
     */
    public function getHide()
    {
        return $this->hide;
    }

    /**
     * Set order
     *
     * @param integer $order
     * @return Message
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer 
     */
    public function getOrder()
    {
        return $this->order;
    }
}
