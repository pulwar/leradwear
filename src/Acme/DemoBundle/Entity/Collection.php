<?php

namespace Acme\DemoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Acme\DemoBundle\Entity\Client
 * 
 * @ORM\Table(name="moda_collection")
 * @ORM\Entity(repositoryClass="Acme\DemoBundle\Repository\CollectionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Collection 
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="ID", type="integer",length=3)
     * @ORM\id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name     
     * @ORM\Column(name="`name`", type="string", length=255, nullable=FALSE)     
     */
    private $name; 

    /**
     * @var string $text
    * @ORM\Column(name="`text`", type="string", length=255, nullable=TRUE)   
     */
    private $text;  

      /**
     * @var string $price
    * @ORM\Column(name="`price`", type="string", length=255, nullable=TRUE)   
     */
    private $price;  

    /**
     * @var integer $hide
     * @ORM\Column(name="`hide`", type="integer", length=3, nullable=TRUE)
     */
    private $hide;

    /**
     * @var integer $order
     * @ORM\Column(name="`order`", type="integer", length=3, nullable=TRUE)
     */
    private $order;    

    /**
    * @ORM\OneToMany(targetEntity="Photo", mappedBy="collection")
    */
    protected $photos;

    /**
    * @ORM\ManyToOne(targetEntity="Category", inversedBy="collections")
    * @ORM\JoinColumn(name="category_id", referencedColumnName="ID")
    */
    protected $category;

    public function __construct()
    {
        $this->photos = new ArrayCollection();       
    }    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Collection
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Collection
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Collection
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set hide
     *
     * @param integer $hide
     * @return Collection
     */
    public function setHide($hide)
    {
        $this->hide = $hide;

        return $this;
    }

    /**
     * Get hide
     *
     * @return integer 
     */
    public function getHide()
    {
        return $this->hide;
    }

    /**
     * Set order
     *
     * @param integer $order
     * @return Collection
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer 
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Add photos
     *
     * @param \Acme\DemoBundle\Entity\Photo $photos
     * @return Collection
     */
    public function addPhoto(\Acme\DemoBundle\Entity\Photo $photos)
    {
        $this->photos[] = $photos;

        return $this;
    }

    /**
     * Remove photos
     *
     * @param \Acme\DemoBundle\Entity\Photo $photos
     */
    public function removePhoto(\Acme\DemoBundle\Entity\Photo $photos)
    {
        $this->photos->removeElement($photos);
    }

    /**
     * Get photos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * Set category
     *
     * @param \Acme\DemoBundle\Entity\Category $category
     * @return Collection
     */
    public function setCategory(\Acme\DemoBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Acme\DemoBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }
}
