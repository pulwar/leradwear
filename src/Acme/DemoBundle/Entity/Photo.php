<?php

namespace Acme\DemoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Acme\DemoBundle\Entity\Client
 * 
 * @ORM\Table(name="moda_photo")
 * @ORM\Entity(repositoryClass="Acme\DemoBundle\Repository\PhotoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Photo 
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="ID", type="integer",length=3)
     * @ORM\id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name     
     * @ORM\Column(name="`name`", type="string", length=255, nullable=FALSE)     
     */
    private $name;     

    /**
     * @var integer $hide
     * @ORM\Column(name="`hide`", type="integer", length=3, nullable=TRUE)
     */
    private $hide;

    /**
     * @var integer $order
     * @ORM\Column(name="`order`", type="integer", length=3, nullable=TRUE)
     */
    private $order;     

    /**
    * @ORM\ManyToOne(targetEntity="Collection", inversedBy="photos")
    * @ORM\JoinColumn(name="collection_id", referencedColumnName="ID")
    */
    protected $collection;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Photo
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set hide
     *
     * @param integer $hide
     * @return Photo
     */
    public function setHide($hide)
    {
        $this->hide = $hide;

        return $this;
    }

    /**
     * Get hide
     *
     * @return integer 
     */
    public function getHide()
    {
        return $this->hide;
    }

    /**
     * Set order
     *
     * @param integer $order
     * @return Photo
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer 
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set collection
     *
     * @param \Acme\DemoBundle\Entity\Collection $collection
     * @return Photo
     */
    public function setCollection(\Acme\DemoBundle\Entity\Collection $collection = null)
    {
        $this->collection = $collection;

        return $this;
    }

    /**
     * Get collection
     *
     * @return \Acme\DemoBundle\Entity\Collection 
     */
    public function getCollection()
    {
        return $this->collection;
    }
}
