<?php

namespace Acme\DemoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Acme\DemoBundle\Entity\Settings
 * 
 * @ORM\Table(name="moda_settings")
 * @ORM\Entity(repositoryClass="Acme\DemoBundle\Repository\MainRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Settings 
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="ID", type="integer",length=3)
     * @ORM\id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;    
   
    /**
     * @var $keywordsMain
    * @ORM\Column(name="keywordsMain", type="text")
     */
    private $keywordsMain;

    /**
     * @var  $descriptionMain
    * @ORM\Column(name="descriptionMain", type="text")
     */
    private $descriptionMain;  

    /**
     * @var $keywordsCollection
    * @ORM\Column(name="keywordsCollection", type="text")
     */
    private $keywordsCollection;

    /**
     * @var $descriptionCollection
    * @ORM\Column(name="descriptionCollection", type="text")  
     */
    private $descriptionCollection;  

    /**
     * @var $keywordsAbout
    * @ORM\Column(name="keywordsAbout", type="text")
     */
    private $keywordsAbout;

    /**
     * @var $descriptionA
    * @ORM\Column(name="descriptionA", type="text")
     */
    private $descriptionA;

     /**
     * @var $keywordsNews
    * @ORM\Column(name="keywordsNews", type="text") 
     */
    private $keywordsNews;

    /**
     * @var $descriptionNews
    * @ORM\Column(name="descriptionNews", type="text")
     */
    private $descriptionNews;

     /**
     * @var $keywordsContacts
    * @ORM\Column(name="keywordsContacts", type="text")
     */
    private $keywordsContacts;

    /**
     * @var $descriptionContacts
    * @ORM\Column(name="descriptionContacts", type="text")
     */
    private $descriptionContacts;     

    /**
     * @var textAbout
     *
     * @ORM\Column(name="textAbout", type="text")
     */
    private $textAbout;

    /**
     * @var textContactsL
     *
     * @ORM\Column(name="textContactsL", type="text")
     */
    private $textContactsL;  

    /**
     * @var textContactsR
     *
     * @ORM\Column(name="textContactsR", type="text")
     */
    private $textContactsR;    

   

    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set keywordsMain
     *
     * @param string $keywordsMain
     * @return Settings
     */
    public function setKeywordsMain($keywordsMain)
    {
        $this->keywordsMain = $keywordsMain;

        return $this;
    }

    /**
     * Get keywordsMain
     *
     * @return string 
     */
    public function getKeywordsMain()
    {
        return $this->keywordsMain;
    }

    /**
     * Set descriptionMain
     *
     * @param string $descriptionMain
     * @return Settings
     */
    public function setDescriptionMain($descriptionMain)
    {
        $this->descriptionMain = $descriptionMain;

        return $this;
    }

    /**
     * Get descriptionMain
     *
     * @return string 
     */
    public function getDescriptionMain()
    {
        return $this->descriptionMain;
    }

    /**
     * Set keywordsCollection
     *
     * @param string $keywordsCollection
     * @return Settings
     */
    public function setKeywordsCollection($keywordsCollection)
    {
        $this->keywordsCollection = $keywordsCollection;

        return $this;
    }

    /**
     * Get keywordsCollection
     *
     * @return string 
     */
    public function getKeywordsCollection()
    {
        return $this->keywordsCollection;
    }

    /**
     * Set descriptionCollection
     *
     * @param string $descriptionCollection
     * @return Settings
     */
    public function setDescriptionCollection($descriptionCollection)
    {
        $this->descriptionCollection = $descriptionCollection;

        return $this;
    }

    /**
     * Get descriptionCollection
     *
     * @return string 
     */
    public function getDescriptionCollection()
    {
        return $this->descriptionCollection;
    }

    /**
     * Set keywordsAbout
     *
     * @param string $keywordsAbout
     * @return Settings
     */
    public function setKeywordsAbout($keywordsAbout)
    {
        $this->keywordsAbout = $keywordsAbout;

        return $this;
    }

    /**
     * Get keywordsAbout
     *
     * @return string 
     */
    public function getKeywordsAbout()
    {
        return $this->keywordsAbout;
    }

    /**
     * Set descriptionA
     *
     * @param string $descriptionA
     * @return Settings
     */
    public function setDescriptionA($descriptionA)
    {
        $this->descriptionA = $descriptionA;

        return $this;
    }

    /**
     * Get descriptionA
     *
     * @return string 
     */
    public function getDescriptionA()
    {
        return $this->descriptionA;
    }

    /**
     * Set keywordsNews
     *
     * @param string $keywordsNews
     * @return Settings
     */
    public function setKeywordsNews($keywordsNews)
    {
        $this->keywordsNews = $keywordsNews;

        return $this;
    }

    /**
     * Get keywordsNews
     *
     * @return string 
     */
    public function getKeywordsNews()
    {
        return $this->keywordsNews;
    }

    /**
     * Set descriptionNews
     *
     * @param string $descriptionNews
     * @return Settings
     */
    public function setDescriptionNews($descriptionNews)
    {
        $this->descriptionNews = $descriptionNews;

        return $this;
    }

    /**
     * Get descriptionNews
     *
     * @return string 
     */
    public function getDescriptionNews()
    {
        return $this->descriptionNews;
    }

    /**
     * Set keywordsContacts
     *
     * @param string $keywordsContacts
     * @return Settings
     */
    public function setKeywordsContacts($keywordsContacts)
    {
        $this->keywordsContacts = $keywordsContacts;

        return $this;
    }

    /**
     * Get keywordsContacts
     *
     * @return string 
     */
    public function getKeywordsContacts()
    {
        return $this->keywordsContacts;
    }

    /**
     * Set descriptionContacts
     *
     * @param string $descriptionContacts
     * @return Settings
     */
    public function setDescriptionContacts($descriptionContacts)
    {
        $this->descriptionContacts = $descriptionContacts;

        return $this;
    }

    /**
     * Get descriptionContacts
     *
     * @return string 
     */
    public function getDescriptionContacts()
    {
        return $this->descriptionContacts;
    }

    /**
     * Set textAbout
     *
     * @param string $textAbout
     * @return Settings
     */
    public function setTextAbout($textAbout)
    {
        $this->textAbout = $textAbout;

        return $this;
    }

    /**
     * Get textAbout
     *
     * @return string 
     */
    public function getTextAbout()
    {
        return $this->textAbout;
    }

    /**
     * Set textContactsL
     *
     * @param string $textContactsL
     * @return Settings
     */
    public function setTextContactsL($textContactsL)
    {
        $this->textContactsL = $textContactsL;

        return $this;
    }

    /**
     * Get textContactsL
     *
     * @return string 
     */
    public function getTextContactsL()
    {
        return $this->textContactsL;
    }

    /**
     * Set textContactsR
     *
     * @param string $textContactsR
     * @return Settings
     */
    public function setTextContactsR($textContactsR)
    {
        $this->textContactsR = $textContactsR;

        return $this;
    }

    /**
     * Get textContactsR
     *
     * @return string 
     */
    public function getTextContactsR()
    {
        return $this->textContactsR;
    }
}
