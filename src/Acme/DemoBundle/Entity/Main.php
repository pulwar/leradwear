<?php

namespace Acme\DemoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Acme\DemoBundle\Entity\Client
 * 
 * @ORM\Table(name="moda_main")
 * @ORM\Entity(repositoryClass="Acme\DemoBundle\Repository\MainRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Main 
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="ID", type="integer",length=3)
     * @ORM\id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name     
     * @ORM\Column(name="`name`", type="string", length=255, nullable=FALSE)     
     */
    private $name; 

    /**
     * @var string $text
    * @ORM\Column(name="`text`", type="string", length=255, nullable=TRUE)   
     */
    private $text;  

    /**
     * @var integer $hide
     * @ORM\Column(name="`hide`", type="integer", length=3, nullable=TRUE)
     */
    private $hide;

    /**
     * @var integer $order
     * @ORM\Column(name="`order`", type="integer", length=3, nullable=TRUE)
     */
    private $order;    

    /**
     * @var string $color
    * @ORM\Column(name="`color`", type="string", length=255, nullable=TRUE)   
     */
    private $color; 

    /**
     * @var string $left
    * @ORM\Column(name="`left`", type="string", length=255, nullable=TRUE)   
     */
    private $left; 

    /**
     * @var string $top
    * @ORM\Column(name="`top`", type="string", length=255, nullable=TRUE)   
     */
    private $top; 

     /**
    * @ORM\ManyToOne(targetEntity="Category", inversedBy="sliders")
    * @ORM\JoinColumn(name="category_id", referencedColumnName="ID")
    */
    protected $category;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Main
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Main
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set hide
     *
     * @param integer $hide
     * @return Main
     */
    public function setHide($hide)
    {
        $this->hide = $hide;

        return $this;
    }

    /**
     * Get hide
     *
     * @return integer 
     */
    public function getHide()
    {
        return $this->hide;
    }

    /**
     * Set order
     *
     * @param integer $order
     * @return Main
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer 
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set color
     *
     * @param string $color
     * @return Main
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string 
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set left
     *
     * @param string $left
     * @return Main
     */
    public function setLeft($left)
    {
        $this->left = $left;

        return $this;
    }

    /**
     * Get left
     *
     * @return string 
     */
    public function getLeft()
    {
        return $this->left;
    }

    /**
     * Set top
     *
     * @param string $top
     * @return Main
     */
    public function setTop($top)
    {
        $this->top = $top;

        return $this;
    }

    /**
     * Get top
     *
     * @return string 
     */
    public function getTop()
    {
        return $this->top;
    }

    /**
     * Set category
     *
     * @param \Acme\DemoBundle\Entity\Category $category
     * @return Main
     */
    public function setCategory(\Acme\DemoBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Acme\DemoBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }
}
