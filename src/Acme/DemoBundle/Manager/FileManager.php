<?php

namespace Acme\DemoBundle\Manager;

use Imagine\Gd\Imagine;
use Imagine\Image\ImageInterface;
use Imagine\Image\Box;

/**
 * FileManager
 */
class FileManager
{
	/**
	 * Check exist directory. Create if not exist.
	 * 
	 * @param  string $directory
	 */
	public function checkDirectory( $directory )
	{
		if (!is_dir($directory)) {
			$oldmask = umask(0);            
			mkdir($directory, 0777);
			umask($oldmask);
		}
	}

	/**
	 * Generate unique file name
	 * @param  string $uploadDir 
	 * @param  string $fileExtension      
	 * @return string
	 */
	public function generateFilename( $uploadDir, $fileExtension )
	{
		$length = 8;
		$characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    
	    do {
		   	$filename = '';
		    for ($i = 0; $i < $length; $i++) {
		        $filename .= $characters[rand(0, strlen($characters) - 1)];
		    }
		} while (file_exists($uploadDir . '/' . $filename . '.' . $fileExtension));

		return $filename;
	}

	/**
     * Build thumbnails for image file and save to disc.
     * 
     * @param  string $file
     * @param  string $filename generated file name
     * @param  string $documentRoot
     * @param  array  $config
     */
    public function buildThumbnails( $file, $filename, $documentRoot, $config )
    {
        // If config not contain need sizes - thumbnails not needed
        if ( !isset($config['imageSizes']) || empty($config['imageSizes'])) {
            return;
        }

        // Get sizes
        $sizes = $config['imageSizes'];

        $imagine = new Imagine();

        foreach ($sizes as $key => $value) {
            //print_r($value); die();
            // Build save directory
            $uploadDir = $documentRoot . $config['directory'] ;//. '/' . $key;
            // Check exist directory, and creation if not exist
            $this->checkDirectory($uploadDir);

            // Fill attributes
            $width   = $value['width'];
            $height  = $value['height'];            
            $mode    = isset($value['mode']) ? $value['mode'] : null;


            // Load image
            $image = $imagine->load($file);
            /*
            if($key == 'ipad_full') {
                //if($image->getSize()->getWidth() / $image->getSize()->getHeight() > $width / $height)
                if($image->getSize()->getWidth() > $image->getSize()->getHeight())
                {
                    $height = $image->getSize()->getHeight();
                    $width = $height; //* ( 4 / 6);
                } else {
                    $width = $image->getSize()->getWidth();
                    $height = $width; // / ( 4 / 6);
                }
            } else if($key == 'ipad_half') {
                if($image->getSize()->getWidth() / $image->getSize()->getHeight() > $width / $height) {
                    //$height = $image->getSize()->getHeight();
                    //$width = $height * ( 16 / 4.5);
                    $height =86;
                    $width =86;                    
                } else {
                    //$width = $image->getSize()->getWidth();
                    //$height = $width / ( 16 / 4.5);
                    $height =86;
                    $width =86;
                }
            }
            */
                    //$height =86;
                    //$width =86;
            
            // Resize
            if (null == $width) {
                $image = $image->resize($image->getSize()->heighten($height));
            } elseif (null == $height) {
                $image = $image->resize($image->getSize()->widen($width));
            } else {
                switch($mode) {
                    case 'outbound':
                        $mode = ImageInterface::THUMBNAIL_OUTBOUND;
                        break;
                    case 'inset':
                        $mode = ImageInterface::THUMBNAIL_INSET;
                        break;
                    default:
                        $mode = ImageInterface::THUMBNAIL_OUTBOUND;
                }
                $image = $image->thumbnail(new Box($width, $height), $mode);
            }

            $filename_arr = explode('.', $filename);
            $filename_ext = array_pop($filename_arr);
            $new_filename = implode('', $filename_arr);
          

            // Save to disc
            $image->save($uploadDir . '/' . $new_filename . '_' . $key . '.' . $filename_ext);
        }
    }
}