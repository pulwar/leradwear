<?php

namespace Acme\DemoBundle\Loader;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraints\File as FileConstraint;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * FileLoader
 */
class FileLoader
{
	/**
     * @var ContainerInterface $container
     */
    private $container;

    /**
     * Context config
     * 
     * @var array
     */
    private $config = array();

    /**
     * Document root directory
     * 
     * @var string
     */
    private $documentRoot;

    /**
     * Constructs a new instance of CookieManager.
     *
     * @param ContainerInterface $container The container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Configure service
     * @param  string $context 
     */
    public function configure($context)
    {
    	$container = $this->container;

    	// get data from config file
    	$param = $container->getParameter('bs_files');
		$defaultConfig = $param['default'];
		$contextConfig = $param['context'];

    	// merge default option with context options
    	$this->config = array_merge($defaultConfig, $contextConfig[$context]);

    	// detect document root directory
    	$this->documentRoot = $container->get('request')->server->get('DOCUMENT_ROOT');
    }

    /**
     * Validate file by conditions from config file
     * @param  UploadedFile $file
     * @return \Symfony\Component\Validator\ConstraintViolationList
     */
    public function validate( UploadedFile $file )
    {
    	$validator = $this->container->get('validator');
		$fileConstraint = new FileConstraint(array(
            'maxSize' => $this->config['maxSize'],
            'mimeTypes' => $this->config['mimeTypes'],
        ));
		$errors = $validator->validateValue($file, $fileConstraint);
		return $errors;
    }

    /**
     * Save file to disc and to db
     * @param  UploadedFile $file
     * @param  boolean $withPersist
     * @return Media
     */
	public function save($file, $filename = '', $withPersist = false)
	{
		$fileManager = $this->container->get('acme.file_manager');
		// Build save directory
		$uploadDir = $this->documentRoot . $this->config['directory'];
		// Check exist directory, and creation if not exist
		$fileManager->checkDirectory($uploadDir);

		if(!$filename) {
			$filename = $fileManager->generateFilename( $uploadDir, $file->guessExtension() );
		} 
		$fullFilename = $filename . '.' . $file->guessExtension();
		
		// If file - image, create and save thumbnails
		if (preg_match("/image\/(.*)/", $file->getClientMimeType()) > 0) {
			$fileManager->buildThumbnails( 
				file_get_contents($file->getPathname()), 
				$fullFilename,
				$this->documentRoot,
				$this->config 
			);
		}

		// Save file
		$file->move($uploadDir, $fullFilename);

		// Build entity
		/*$media = new Media();
		$media
			->setTitle( $file->getClientOriginalName() )
			->setMimeType( $file->getClientMimeType() )
			->setFilename( $fullFilename )
			->setSize( $file->getClientSize() )
			->setIsEmbed( false )
		;

		// Save entity
		if ( $withPersist ) {
			$em = $this->container->get('doctrine')->getEntityManager();
			$em->persist($media);
			$em->flush();	
		}*/

		return $fullFilename;
	}
	

	/**
	 * Remove file and all thumbs from disc
	 * @param  Media $file
	 */
	public function remove($file)
	{
		$uploadDir = $this->documentRoot . $this->config['directory'];
		$filename = $file; //$file->getFilename();

		@unlink($uploadDir . '/' . $filename);

		if ( !isset($this->config['imageSizes']) || empty($this->config['imageSizes'])) {
			return;
		}
		$sizes = $this->config['imageSizes'];
		foreach ($sizes as $key => $value) {
			// Build save directory
			$uploadDir = $this->documentRoot . $this->config['directory'] . '/' . $key;

			@unlink($uploadDir . '/' . $filename);
		}
		return;
	}
}