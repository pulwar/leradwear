<?php

namespace Acme\DemoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Acme\DemoBundle\Entity\Collection;
use Acme\DemoBundle\Entity\Photo;
use Acme\DemoBundle\Form\CollectionType;
use Acme\DemoBundle\Form\PhotoType;

/**
 * Collection controller.
 *
 */
class PhotoController extends Controller
{    
    /**
     * Creates a new Collection entity.
     *
     */
    public function createAction(Request $request)
    {        
        $em = $this->getDoctrine()->getManager();
        $collection = $em->getRepository('AcmeDemoBundle:Collection')->find($request->get('id')); 
        //echo count($collection); die();       
        $entity = new Photo();             
        
        $fileLoader = $this->get('acme.file_loader');
        $fileLoader->configure('photo');            
        $file = $request->files->get('image'); 

        if(!count($file)) {
            $errors['level1']['backgroud_image'] = 'File required';
        } else if(count($file)) { // validate
            $file_errors = $fileLoader->validate($file);
            if (count($file_errors) > 0) {
                $errors['level1']['sections']['backgroud_image'][$record_num] = $file_errors[0]->getMessage();
            } else {                    
                $file_name = $fileLoader->save($file);                                
            }
        }

        $entity->setName($file_name);  
        $entity->setCollection($collection);           
        $em->persist($entity);        
        //$entity->setCollection($collection);   
        $em->flush();       
        
        return $this->redirect($this->generateUrl('collection_edit', array('id' => $request->get('id'))));
        

        
    }

    /**
    * Creates a form to create a Photo entity.
    *
    * @param Photo $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Photo $entity)
    {
        $form = $this->createForm(new PhotoType(), $entity, array(
            'action' => $this->generateUrl('photo_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Deletes a Photo entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {        
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AcmeDemoBundle:Photo')->find($id);
        $name = $entity->getName();
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Photo entity.');
        }
        $small=explode(".",$name,2);
        $nameSmall = $small[0].'_small.'.$small[1];             
        $file = $_SERVER['DOCUMENT_ROOT'].'/bundles/acmedemo/img/photo/'.$name; 
        $fileSmall =  $_SERVER['DOCUMENT_ROOT'].'/bundles/acmedemo/img/photo/'.$nameSmall; 
        unlink($file); 
        unlink($fileSmall);
        $em->remove($entity);
        $em->flush();
        
        
        return $this->redirect($this->generateUrl('collection_edit', array('id' => $request->get('collectionId'))));
    }    
}
