<?php

namespace Acme\DemoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Acme\DemoBundle\Entity\Collection;
use Acme\DemoBundle\Form\CollectionType;

/**
 * Collection controller.
 *
 */
class CollectionController extends Controller
{

    /**
     * Lists all Collection entities.
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();        
        $entities = $em->getRepository('AcmeDemoBundle:Collection')->findAllOrder();

        return $this->render('AcmeDemoBundle:Collection:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Collection entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Collection();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            //file add
            $fileLoader = $this->get('acme.file_loader');
            $fileLoader->configure('collection');            
            $file = $request->files->get('image'); 

            if(!count($file)) {
                $errors['level1']['backgroud_image'] = 'File required';
            } else if(count($file)) { // validate
                $file_errors = $fileLoader->validate($file);
                if (count($file_errors) > 0) {
                    $errors['level1']['sections']['backgroud_image'][$record_num] = $file_errors[0]->getMessage();
                } else {                    
                    $file_name = $fileLoader->save($file);                                
                }
            }

            //$name=explode(".", $file_name);
            //$file_name=''.$name[0].'_small.'.$name[1].'';           
            $entity->setName($file_name); 

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('collection'));
        }

        return $this->render('AcmeDemoBundle:Main:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Collection entity.
    *
    * @param Collection $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Collection $entity)
    {
        $form = $this->createForm(new CollectionType(), $entity, array(
            'action' => $this->generateUrl('collection_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Collection entity.
     *
     */
    public function newAction()
    {
        $entity = new Collection();
        $form   = $this->createCreateForm($entity);

        return $this->render('AcmeDemoBundle:Collection:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Collection entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AcmeDemoBundle:Collection')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Collection entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AcmeDemoBundle:Collection:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Collection entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AcmeDemoBundle:Collection')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Collection entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AcmeDemoBundle:Collection:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function upAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $img = $em->getRepository('AcmeDemoBundle:Collection')->findOneById($id);
        $order=$img->getOrder()+1;
        $imgUp = $em->getRepository('AcmeDemoBundle:Collection')->findOneByOrder($order);
        if($imgUp){
            $img->setOrder($order);
            $em->persist($img);
            $em->flush();

            $imgUp->setOrder($order-1);
            $em->persist($imgUp);
            $em->flush();
        }
        
        return $this->redirect($this->generateUrl('collection'));
    }

     public function downAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $img = $em->getRepository('AcmeDemoBundle:Collection')->findOneById($id);
        $order=$img->getOrder()-1;
        $imgUp = $em->getRepository('AcmeDemoBundle:Collection')->findOneByOrder($order);
        if($imgUp){
            $img->setOrder($order);
            $em->persist($img);
            $em->flush();

            $imgUp->setOrder($order+1);
            $em->persist($imgUp);
            $em->flush();
        }
        
        return $this->redirect($this->generateUrl('collection'));
    }
    /**
    * Creates a form to edit a Collection entity.
    *
    * @param Collection $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Collection $entity)
    {
        $form = $this->createForm(new CollectionType(), $entity, array(
            'action' => $this->generateUrl('collection_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Collection entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AcmeDemoBundle:Collection')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Collection entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('collection_edit', array('id' => $id)));
        }

        return $this->render('AcmeDemoBundle:Collection:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Collection entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AcmeDemoBundle:Collection')->find($id);
            $name = $entity->getName();
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Collection entity.');
            }
            $small=explode(".",$name,2);
            $nameSmall = $small[0].'_small.'.$small[1];             
            $file = $_SERVER['DOCUMENT_ROOT'].'/bundles/acmedemo/img/collection/'.$name; 
            $fileSmall =  $_SERVER['DOCUMENT_ROOT'].'/bundles/acmedemo/img/collection/'.$nameSmall; 
            unlink($file); 
            unlink($fileSmall);
            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('collection'));
    }

    /**
     * Creates a form to delete a Collection entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('collection_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
