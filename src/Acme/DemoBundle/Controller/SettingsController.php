<?php

namespace Acme\DemoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Acme\DemoBundle\Entity\Settings;
use Acme\DemoBundle\Form\SettingsType;

/**
 * Settings controller.
 *
 */
class SettingsController extends Controller
{

    /**
     * Lists all Settings entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AcmeDemoBundle:Settings')->findAll();

        return $this->render('AcmeDemoBundle:Settings:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Settings entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Settings();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_settings_show', array('id' => $entity->getId())));
        }

        return $this->render('AcmeDemoBundle:Settings:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Settings entity.
    *
    * @param Settings $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Settings $entity)
    {
        $form = $this->createForm(new SettingsType(), $entity, array(
            'action' => $this->generateUrl('admin_settings_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Settings entity.
     *
     */
    public function newAction()
    {
        $entity = new Settings();
        $form   = $this->createCreateForm($entity);

        return $this->render('AcmeDemoBundle:Settings:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Settings entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AcmeDemoBundle:Settings')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Settings entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AcmeDemoBundle:Settings:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Settings entity.
     *
     */
    public function editAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AcmeDemoBundle:Settings')->find(1);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Settings entity.');
        }

        $editForm = $this->createEditForm($entity);
        

        return $this->render('AcmeDemoBundle:Settings:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            
        ));
    }

    /**
    * Creates a form to edit a Settings entity.
    *
    * @param Settings $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Settings $entity)
    {
        $form = $this->createForm(new SettingsType(), $entity, array(
            'action' => $this->generateUrl('admin_settings_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Settings entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AcmeDemoBundle:Settings')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Settings entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin_settings', array('id' => $id)));
        }

        return $this->render('AcmeDemoBundle:Settings:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Settings entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AcmeDemoBundle:Settings')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Settings entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_settings'));
    }

    /**
     * Creates a form to delete a Settings entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_settings_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
