<?php

namespace Acme\DemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
		  $em = $this->getDoctrine()->getManager();      
      	$images = $em->getRepository('AcmeDemoBundle:Main')->findBy(array(), array('order' => 'ASC'));
        $meta = $em->getRepository('AcmeDemoBundle:Settings')->find(1);       	
      return $this->render('AcmeDemoBundle:Default:main.html.twig', array(
          'images'=>$images,
          'meta'=>$meta ));
    }
    public function aboutAction(Request $request)
    {    
      $em = $this->getDoctrine()->getManager();      
        $content = $em->getRepository('AcmeDemoBundle:About')->findOneById(1); 
        $settings = $em->getRepository('AcmeDemoBundle:Settings')->findOneById(1); 
      return $this->render('AcmeDemoBundle:Default:about.html.twig', array(
          'content'=>$content,
          'settings'=>$settings,
          ));
    }
    public function newsAction(Request $request)
    {       
      $em = $this->getDoctrine()->getManager();      
      $news = $em->getRepository('AcmeDemoBundle:News')->findAll();    
      $meta = $em->getRepository('AcmeDemoBundle:Settings')->find(1);   
      return $this->render('AcmeDemoBundle:Default:news.html.twig', array(
          'news'=>$news,
          'meta'=>$meta));
    }
    public function collectionAction(Request $request)
    {   
      $url = $request->get('url');
      $em = $this->getDoctrine()->getManager();      
      $category = $em->getRepository('AcmeDemoBundle:Category')->findBy(array('url'=>$url));

      $images = $em->getRepository('AcmeDemoBundle:Collection')->findAllOrder($category[0]->getId());       
      
      $meta = $em->getRepository('AcmeDemoBundle:Settings')->find(1); 
      return $this->render('AcmeDemoBundle:Default:collection.html.twig', array(
          'images'=>$images,
          'meta'=>$meta,
          'category'=>$category[0],
          ));
    }
    public function contactsAction(Request $request)
    { 
      $em = $this->getDoctrine()->getManager(); 
      $settings = $em->getRepository('AcmeDemoBundle:Settings')->findOneById(1);   
      $meta = $em->getRepository('AcmeDemoBundle:Settings')->find(1); 
      return $this->render('AcmeDemoBundle:Default:contacts.html.twig', array(          
          'settings'=>$settings,
          'meta'=>$meta
          ));
    }
    public function modaAction(){
      return $this->redirect($this->generateUrl('admin_main'));
    }

    public function photoAction(Request $request, $id){
      $em = $this->getDoctrine()->getManager(); 
      $url = $request->get('url');
      $dress = $em->getRepository('AcmeDemoBundle:Collection')->findOneById($id);
      $category = $em->getRepository('AcmeDemoBundle:Category')->findBy(array('url'=>$url));      
      return $this->render('AcmeDemoBundle:Default:shop.html.twig', array(
        'dress' => $dress,
        'category' => $category[0],
        ));  
    }

     public function collectionMenuAction(Request $request)
    { 
      $em = $this->getDoctrine()->getManager(); 
      $categories = $em->getRepository('AcmeDemoBundle:Category')->findBy(array(), array( 'order' => 'ASC' ));
      return $this->render('AcmeDemoBundle:Default:collection_list.html.twig', array(          
          'categories'=>$categories,          
          ));
    }
}