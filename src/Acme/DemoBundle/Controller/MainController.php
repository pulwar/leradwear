<?php

namespace Acme\DemoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Acme\DemoBundle\Entity\Main;
use Acme\DemoBundle\Form\MainType;

/**
 * Main controller.
 *
 */
class MainController extends Controller
{

    /**
     * Lists all Main entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AcmeDemoBundle:Main')->findBy(array(), array( 'order' => 'ASC' ));

        return $this->render('AcmeDemoBundle:Main:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Main entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Main();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            //file add
            $fileLoader = $this->get('acme.file_loader');
            $fileLoader->configure('avatars');            
            $file = $request->files->get('image'); 

            if(!count($file)) {
                $errors['level1']['backgroud_image'] = 'File required';
            } else if(count($file)) { // validate
                $file_errors = $fileLoader->validate($file);
                if (count($file_errors) > 0) {
                    $errors['level1']['sections']['backgroud_image'][$record_num] = $file_errors[0]->getMessage();
                } else {                    
                    $file_name = $fileLoader->save($file);                                
                }
            }

            //$name=explode(".", $file_name);
            //$file_name=''.$name[0].'_small.'.$name[1].'';           
            $entity->setName($file_name); 

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_main_show', array('id' => $entity->getId())));
        }

        return $this->render('AcmeDemoBundle:Main:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Main entity.
    *
    * @param Main $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Main $entity)
    {
        $form = $this->createForm(new MainType(), $entity, array(
            'action' => $this->generateUrl('admin_main_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Main entity.
     *
     */
    public function newAction()
    {
        $entity = new Main();
        $form   = $this->createCreateForm($entity);

        return $this->render('AcmeDemoBundle:Main:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Main entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AcmeDemoBundle:Main')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Main entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AcmeDemoBundle:Main:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Main entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AcmeDemoBundle:Main')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Main entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AcmeDemoBundle:Main:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Main entity.
    *
    * @param Main $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Main $entity)
    {
        $form = $this->createForm(new MainType(), $entity, array(
            'action' => $this->generateUrl('admin_main_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Main entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AcmeDemoBundle:Main')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Main entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin_main_edit', array('id' => $id)));
        }

        return $this->render('AcmeDemoBundle:Main:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Main entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AcmeDemoBundle:Main')->find($id);
            $name = $entity->getName();
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Main entity.');
            }
            $small=explode(".",$name,2);
            $nameSmall = $small[0].'_small.'.$small[1];             
            $file = $_SERVER['DOCUMENT_ROOT'].'/bundles/acmedemo/img/main/'.$name; 
            $fileSmall =  $_SERVER['DOCUMENT_ROOT'].'/bundles/acmedemo/img/main/'.$nameSmall; 
            unlink($file); 
            unlink($fileSmall);
            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_main'));
    }

    /**
     * Creates a form to delete a Main entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_main_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
     public function uploadImageAction()
    {
    $callback = $_GET['CKEditorFuncNum'];
    $file_name = $_FILES['upload']['name'];
    $file_name_tmp = $_FILES['upload']['tmp_name'];
    $file_new_name = $_SERVER['DOCUMENT_ROOT'].'/bundles/acmedemo/img/upload/';
   
    
     $sExtension =  strrchr($_FILES['upload']['name'],'.');
     
     $file_name = md5(rand(10000,99999));
     //для локалки
    //$full_path = 'D:/Xamp/htdocs/Symfony/web/bundles/course/img/upload/'.basename($file_name.$sExtension);
    //$http_path = 'http://127.0.0.1/symfony/web/bundles/course/img/upload/'.$file_name.$sExtension;    
    
    //для сервера
    $full_path =  $file_new_name.basename($file_name.$sExtension);
    $http_path = '/bundles/acmedemo/img/upload/'.$file_name.$sExtension;    
    /*
    echo "</br>";
    echo $full_path; //die();    
    echo "</br>";
    echo $http_path;die();
    */
    //echo "!!!!";
    //cho file_exists($file_name_tmp)? 'yes':'no';
    
    //echo $file_name_tmp; die();
    $error = '';
    if( move_uploaded_file($file_name_tmp, $full_path) ) {        
    } else {         
     $error = 'Some error occured please try again later';
     $http_path = '';
    }
    echo "<script type=\"text/javascript\">window.parent.CKEDITOR.tools.callFunction(".$callback.",  \"".$http_path."\", \"".$error."\" );</script>";

      //return $this->redirect($this->generateUrl('coursetable'));
    }

    public function upAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $img = $em->getRepository('AcmeDemoBundle:Main')->findOneById($id);
        $order=$img->getOrder()+1;
        $imgUp = $em->getRepository('AcmeDemoBundle:Main')->findOneByOrder($order);
        if($imgUp){
            $img->setOrder($order);
            $em->persist($img);
            $em->flush();

            $imgUp->setOrder($order-1);
            $em->persist($imgUp);
            $em->flush();
        }
        
        return $this->redirect($this->generateUrl('admin_main'));
    }

     public function downAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $img = $em->getRepository('AcmeDemoBundle:Main')->findOneById($id);
        $order=$img->getOrder()-1;
        $imgUp = $em->getRepository('AcmeDemoBundle:Main')->findOneByOrder($order);
        if($imgUp){
            $img->setOrder($order);
            $em->persist($img);
            $em->flush();

            $imgUp->setOrder($order+1);
            $em->persist($imgUp);
            $em->flush();
        }
        
        return $this->redirect($this->generateUrl('admin_main'));
    }
}
