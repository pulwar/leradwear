<?php

namespace Acme\DemoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CollectionType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder            
            ->add('category','entity',array('class'=>'AcmeDemoBundle:Category','multiple'=>false, 'label'  => 'Коллекция'))
            ->add('text','text',array('label'  => 'Описание','required'  => false))
            ->add('price','text',array('label'  => 'Цена','required'  => false)) 
            ->add('order','text',array('label'  => 'Порядок','required'  => false))            
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Acme\DemoBundle\Entity\Collection'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'acme_demobundle_collection';
    }
}
