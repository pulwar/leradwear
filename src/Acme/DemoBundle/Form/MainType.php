<?php

namespace Acme\DemoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MainType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder            
            ->add('category','entity',array('class'=>'AcmeDemoBundle:Category','multiple'=>false, 'label'  => 'Коллекция'))
            ->add('order','text',array('label'  => 'Порядок','required'  => false))
            ->add('text','text',array('label'  => 'Описание','required'  => false))
            ->add('color','text',array('label'  => 'Цвет текста','required'  => false))
            ->add('left','text',array('label'  => 'Горизонтальное рас-ие','required'  => false))
            ->add('top','text',array('label'  => 'Вертикальное рас-ие','required'  => false))            
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Acme\DemoBundle\Entity\Main'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'acme_demobundle_main';
    }
}
