<?php

namespace Acme\DemoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SettingsType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('keywordsMain')
            ->add('descriptionMain')
            ->add('keywordsCollection')
            ->add('descriptionCollection')
            ->add('keywordsAbout')
            ->add('descriptionA')
            ->add('keywordsNews')
            ->add('descriptionNews')
            ->add('keywordsContacts')
            ->add('descriptionContacts')           
            ->add('textAbout','textarea',array('label'  => 'О нас:','attr'=>array('class'=>'ckeditor'),'required' => TRUE))
            ->add('textContactsL','textarea',array('label'  => 'Контакты слева:','attr'=>array('class'=>'ckeditor'),'required' => TRUE))
            ->add('textContactsR','textarea',array('label'  => 'Контакты срава:','attr'=>array('class'=>'ckeditor'),'required' => TRUE))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Acme\DemoBundle\Entity\Settings'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'acme_demobundle_settings';
    }
}
