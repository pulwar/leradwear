<?php

namespace Acme\DemoBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;


class CollectionRepository extends EntityRepository
{
	
	public function findAllOrder($categoryId = null)
	{

		$qb = $this->createQueryBuilder('Collection');
		$qb->select('Collection, Category')	
			->leftJoin('Collection.category','Category');
			if($categoryId) {
				$qb->where('Category.id = :categoryId')
				->setParameter('categoryId', $categoryId);
			}
					
			$qb	
			->orderBy('Collection.order','ASC');
			
		try {			
			return $qb->getQuery()->getResult();							
		} catch (\Doctrine\ORM\NoResultException $e) {
			return null;
		}
	}

}