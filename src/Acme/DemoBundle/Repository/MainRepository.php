<?php

namespace Acme\DemoBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;


class MainRepository extends EntityRepository
{
	public function findClients($type,$country,$FIO,$mail,$sortingBy,$sortingAs,$limit,$page,$c=0) {
		$countryType = array('0' =>'Беларусь','1' =>'Россия','2' =>'Другая',);

        if($country!=''){
        	$country=$countryType[$country];
        }

		$qb = $this->createQueryBuilder('Client');
		$qb->select('Client')
			//->leftJoin('OrderHistory.user', 'user')			
			->orderBy('Client.id', 'DESC');

		if( $type != array('0' => '' )) {
			$qb->andWhere('Client.FlagProfessional IN (:type)')			
			->setParameter('type', $type);
		}
		
		if( $country != '') {
			//var_export( $countryType[$country[0]]); die();
			$qb->andWhere('Client.Country IN (:country)')
			->setParameter('country', $country);
		}
		
		if( $FIO != '') {
			//var_export( $countryType[$country[0]]); die();
			$qb->andWhere("Client.Name LIKE '%".$FIO."%'");
			//$qb->andWhere("Client.Name LIKE '%(:FIO)%'")			
			//->setParameter('FIO', $FIO);
		}
		if( $mail != '') {
			//var_export( $countryType[$country[0]]); die();
			$qb->andWhere("Client.Email LIKE '%".$mail."%'");
			//$qb->andWhere("Client.Name LIKE '%(:FIO)%'")			
			//->setParameter('FIO', $FIO);
		}	
			
		if( $c != 0 ){
			//count of clients on all pages	
		}else{
			$qb
			->setFirstResult($limit*($page-1))
        	->setMaxResults($limit);	
		}
			    
		try {
			if($c!=0){
				return count($qb->getQuery()->getResult());	
			}else{
				return $qb->getQuery()->getResult();	
			}			
		} catch (\Doctrine\ORM\NoResultException $e) {
			return null;
		}

	}

	public function findClientWithOrders($id)
	{
		$qb = $this->createQueryBuilder('Client');
		$qb->select('Client,orders')
			->leftJoin('Client.orders','orders')
			->andWhere('Client.id IN (:id)')			
			->setParameter('id', $id);	
		try {			
			return $qb->getQuery()->getSingleResult();							
		} catch (\Doctrine\ORM\NoResultException $e) {
			return null;
		}
	}

}