<?php

namespace Acme\DemoBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;


class CategoryRepository extends EntityRepository
{
	
	public function findAllOrder()
	{
		$qb = $this->createQueryBuilder('Collection');
		$qb->select('Collection')					
			->orderBy('Collection.order','ASC');
		try {			
			return $qb->getQuery()->getResult();							
		} catch (\Doctrine\ORM\NoResultException $e) {
			return null;
		}
	}

}