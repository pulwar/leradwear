-- MySQL dump 10.13  Distrib 5.5.27, for Win32 (x86)
--
-- Host: localhost    Database: moda
-- ------------------------------------------------------
-- Server version	5.5.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `fos_user`
--

DROP TABLE IF EXISTS `fos_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fos_user`
--

LOCK TABLES `fos_user` WRITE;
/*!40000 ALTER TABLE `fos_user` DISABLE KEYS */;
INSERT INTO `fos_user` VALUES (1,'pulwar','pulwar','kingmax@inbox.ru','kingmax@inbox.ru',1,'iiqgomf3r54o48sosk4swwcwkc0sg4c','frPZbI50Gb3mOzhW5rafZzXqEFn1PwFEGr+gIFOGwQLe2Bezt5XFQ+UORwAV9nVkRM+JaEEb2JaD+tR3YjY5Zg==','2015-05-31 21:31:49',0,0,NULL,NULL,NULL,'a:0:{}',0,NULL);
/*!40000 ALTER TABLE `fos_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moda_about`
--

DROP TABLE IF EXISTS `moda_about`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moda_about` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `hide` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moda_about`
--

LOCK TABLES `moda_about` WRITE;
/*!40000 ALTER TABLE `moda_about` DISABLE KEYS */;
INSERT INTO `moda_about` VALUES (1,'ABOUT US','page2_pic1.jpg','','FUSCE EUISMOD CONSEQUAT ANTE. LOREM IPSUM DOLOR SIT AMET, CONSECTETUER ADIPISCING ELIT. PELLENTESQUE SED DOLOR. ALIQUAM CONGUE\nIN JUSTO EST, SOLLICITUDIN EU SCELERISQUE PRETIUM, PLACERAT EGET ELIT. PRAESENT FAUCIBUS RUTRUM ODIO AT RHONCUS. PELLENTESQUE VITAE TORTOR ID NEQUE FERMENTUM PRETIUM. MAECENAS AC LACUS UT NEQUE RHONCUS LAOREET SED.\nID TELLUS. DONEC JUSTO TELLUS, TINCIDUNT VITAE PELLENTESQUE NEC, PHARETRA A ORCI. PRAESENT NUNC NUNC, EGESTAS EGET ELEMENTUM SED; RUTRUM EGET METUS! VESTIBULUM CONGUE CONGUE DUI UT PORTA. AENEAN LAOREET VIVERRA TURPIS, A COMMODO PURUS ELEIFEND A.',NULL,NULL);
/*!40000 ALTER TABLE `moda_about` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moda_collection`
--

DROP TABLE IF EXISTS `moda_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moda_collection` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hide` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moda_collection`
--

LOCK TABLES `moda_collection` WRITE;
/*!40000 ALTER TABLE `moda_collection` DISABLE KEYS */;
INSERT INTO `moda_collection` VALUES (11,'TkkZqkGt.jpeg','Модель: Арабески',NULL,1,NULL),(12,'eDubtYEu.jpeg','Модель: Марго',NULL,2,NULL),(13,'pYVdHEWo.jpeg','Модель: Акварель',NULL,3,NULL),(18,'ajFNnRQz.jpeg','Модель: Мозаика',NULL,4,NULL),(19,'JccfMQub.jpeg','Модель: Розевиль',NULL,6,NULL),(22,'UkIakTJK.jpeg','Модель: Палермо',NULL,5,NULL),(23,'BlFRasxI.jpeg','Модель: Бали',NULL,8,NULL),(24,'BBzemNOi.jpeg','Модель: Синий микс',NULL,9,NULL),(25,'qgbtueuw.jpeg','Модель: Ника',NULL,10,NULL),(26,'LjcdfOLl.jpeg','Модель: Ясмин',NULL,7,NULL);
/*!40000 ALTER TABLE `moda_collection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moda_main`
--

DROP TABLE IF EXISTS `moda_main`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moda_main` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hide` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `left` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `top` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moda_main`
--

LOCK TABLES `moda_main` WRITE;
/*!40000 ALTER TABLE `moda_main` DISABLE KEYS */;
INSERT INTO `moda_main` VALUES (18,'gqxLpCjz.jpeg',NULL,NULL,NULL,'red','10%','20%'),(20,'QeYUukXw.jpeg',NULL,NULL,NULL,NULL,NULL,NULL),(29,'qcJmCpbu.jpeg',NULL,NULL,NULL,NULL,NULL,NULL),(30,'XQUNxMPn.jpeg',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `moda_main` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moda_message`
--

DROP TABLE IF EXISTS `moda_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moda_message` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Stamp` datetime NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `hide` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moda_message`
--

LOCK TABLES `moda_message` WRITE;
/*!40000 ALTER TABLE `moda_message` DISABLE KEYS */;
INSERT INTO `moda_message` VALUES (1,'test','test','123123','2014-05-10 17:38:06','desc',NULL,NULL),(38,'','','','2014-08-20 02:23:36','',NULL,NULL),(39,'','','','2014-08-27 10:30:32','',NULL,NULL),(40,'','','','2014-09-10 13:18:32','',NULL,NULL),(41,'','','','2014-09-24 00:46:21','',NULL,NULL),(42,'','','','2014-10-07 20:06:40','',NULL,NULL),(43,'','','','2014-11-02 07:37:13','',NULL,NULL),(44,'','','','2015-03-05 13:54:06','',NULL,NULL),(45,'','','','2015-04-11 17:10:23','',NULL,NULL),(46,'Gary Mitchell','garymitchell016@gmail.com','0120120120','2015-04-17 13:46:45','Want more clients and customers? We will help them find you by putting you on the 1st page of Google. Email us back to get a full proposal.',NULL,NULL);
/*!40000 ALTER TABLE `moda_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moda_news`
--

DROP TABLE IF EXISTS `moda_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moda_news` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Stamp` datetime NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `hide` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moda_news`
--

LOCK TABLES `moda_news` WRITE;
/*!40000 ALTER TABLE `moda_news` DISABLE KEYS */;
INSERT INTO `moda_news` VALUES (3,NULL,'2014-05-25 15:08:52','<h2>Первая коллекция от LERAD</h2>\r\n\r\n<div class=\"text-news\" style=\"border: 0px none; padding: 0px; margin: 0px; outline: none; text-align: justify; color: rgb(119, 119, 119); font-family: OpenSansSemibold, Verdana, Geneva, sans-serif; font-size: 12px; line-height: 17px;\">\r\n<p>Мы рады представить вам нашу первую коллекцию 2014 года. Весна/Лето от LERAD отличается большим обилием красок и контрастов. <span style=\"color:rgb(119, 119, 119); font-family:opensanssemibold,verdana,geneva,sans-serif; font-size:12px\">Наш дизайнер воплотил в этой коллекции все свои самые креативные идеи, сделав не просто красивые вещи, но по-настоящему удобные, которые действительно хочется носить.</span></p>\r\n\r\n<p>В коллекции ВЕСНА\\ЛЕТО 2014 вы сможете найти не только вечерние платья для серьёзных мероприятий, но и яркие, лёгкие, по-настоящему летние платья, которые будут радовать вас каждый день и станут незаменимой частью вашего гардероба и вашей жизни.</p>\r\n\r\n<p>При пошиве наших платьев мы использовали дизайнерский шёлк известных итальянских производителей, которые всегда знают толк в том, что будет актуально в этом сезоне. Яркость палитры и богатство рисунка позволили нам создать нечто новое и интересное, что позволит вам всегда быть в центре внимания.</p>\r\n</div>',NULL,NULL),(8,NULL,'2015-02-15 00:24:41','<style type=\"text/css\">html, body,.container {	    \r\n	    min-height: 100%;\r\n	}\r\n.container{\r\nheight: 510px;\r\npadding-top: 5px;\r\npadding-right: 35px;\r\n}\r\n.container img {\r\n		width: 99.7%;\r\n	}\r\n	.first {\r\n    float: left;\r\n    width: 33%;\r\n    height: 50%;    \r\n}\r\n\r\n.second{\r\n    float: left;\r\n    width: 33%;\r\n    height: 50%;    \r\n}\r\n\r\n\r\n.third{\r\n    float: right;\r\n    width: 66.66%;\r\n    height: 100%;    \r\n}\r\n\r\n.fourth {\r\n    float: right;\r\n    width: 99%;\r\n    height: 50%;    \r\n}\r\n\r\n.last{\r\n    float: right;\r\n    width: 99%;\r\n    height: 50%;    \r\n}\r\n</style>\r\n<script type=\"text/javascript\" src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js\"></script>\r\n<link href=\"/bundles/acmedemo/css/jquery.fancybox.css\" media=\"screen\" rel=\"stylesheet\" type=\"text/css\" /><script type=\"text/javascript\" src=\"/bundles/acmedemo/js/jquery.fancybox.pack.js\"></script>\r\n<h2 style=\"text-align:justify\">Краткий фотоотчтёт со съёмок коллекции Весна/Лето 2014</h2>\r\n\r\n<p><span style=\"font-family:verdana,geneva,sans-serif\"><span style=\"color:rgb(119, 119, 119); font-size:12px\">Выражаем большую благодарность Яну и Миле Крюковым за их помощь, чудесные фотографии и хорошее настроение подаренное нам во время съёмок :) Благодаря ним вы можете увидеть нашу первую коллекцию, эти яркие краски и силуэты. </span></span></p>\r\n\r\n<p><span style=\"font-family:verdana,geneva,sans-serif\"><span style=\"color:rgb(119, 119, 119); font-size:12px\">Они принесли в них частичку себя, своё восприятие, своё видение нашей коллекции. Надеюсь, они снова и снова будут радовать нас &nbsp;своими новыми работами и, разумеется, не забудут о нас при выходе следующей коллекции.</span></span></p>\r\n\r\n<div class=\"container\">\r\n<div style=\"float: left;\r\nwidth: 75%;min-height: 100%;\">\r\n<div class=\"first\"><a class=\"fancybox\" href=\"/bundles/acmedemo/img/upload/images/FullSizeRender.jpg\" rel=\"gallery\"><img src=\"/bundles/acmedemo/img/upload/images/FullSizeRender.jpg\" /> </a></div>\r\n\r\n<div class=\"third\"><a class=\"fancybox\" href=\"/bundles/acmedemo/img/upload/images/FullSizeRender_1.jpg\" rel=\"gallery\"><img src=\"/bundles/acmedemo/img/upload/images/FullSizeRender_1.jpg\" /> </a></div>\r\n\r\n<div class=\"second\"><a class=\"fancybox\" href=\"/bundles/acmedemo/img/upload/images/FullSizeRender_2.jpg\" rel=\"gallery\"><img src=\"/bundles/acmedemo/img/upload/images/FullSizeRender_2.jpg\" /> </a></div>\r\n</div>\r\n\r\n<div style=\"float: right;\r\nwidth: 25%;min-height: 100%;\">\r\n<div class=\"fourth\"><a class=\"fancybox\" href=\"/bundles/acmedemo/img/upload/images/FullSizeRender_3.jpg\" rel=\"gallery\"><img src=\"/bundles/acmedemo/img/upload/images/FullSizeRender_3.jpg\" /> </a></div>\r\n\r\n<div class=\"last\"><a class=\"fancybox\" href=\"/bundles/acmedemo/img/upload/images/FullSizeRender_4.jpg\" rel=\"gallery\"><img src=\"/bundles/acmedemo/img/upload/images/FullSizeRender_4.jpg\" /> </a></div>\r\n</div>\r\n</div>\r\n<script>\r\n    $(document).ready(function() {\r\n        $(\'.fancybox\').fancybox({\r\n        	helpers:  {\r\n		        overlayShow : true, //затемнение основной страницы (фон) true/false\r\n				overlayOpacity : 0.8, //прозрачность фона\r\n				overlayColor : \'#777\', //цвет фона\r\n		    }\r\n    	});\r\n    });\r\n</script>',NULL,NULL),(11,NULL,'2015-04-26 22:18:34','<style type=\"text/css\">html, body,.container {	    \r\n	    min-height: 100%;\r\n	}\r\n.container{\r\nheight: 510px;\r\npadding-top: 5px;\r\npadding-right: 35px;\r\n}\r\n.container img {\r\n		width: 99.7%;\r\n	}\r\n	.first {\r\n    float: left;\r\n    width: 33%;\r\n    height: 50%;    \r\n}\r\n\r\n.second{\r\n    float: left;\r\n    width: 33%;\r\n    height: 50%;    \r\n}\r\n\r\n\r\n.third{\r\n    float: right;\r\n    width: 66.66%;\r\n    height: 100%;    \r\n}\r\n\r\n.fourth {\r\n    float: right;\r\n    width: 99%;\r\n    height: 50%;    \r\n}\r\n\r\n.last{\r\n    float: right;\r\n    width: 99%;\r\n    height: 50%;    \r\n}\r\n</style>\r\n<script type=\"text/javascript\" src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js\"></script>\r\n<link href=\"/bundles/acmedemo/css/jquery.fancybox.css\" media=\"screen\" rel=\"stylesheet\" type=\"text/css\" /><script type=\"text/javascript\" src=\"/bundles/acmedemo/js/jquery.fancybox.pack.js\"></script>\r\n<h2>Наш офис<span style=\"font-size:13px\">&nbsp;&nbsp;</span><span style=\"font-size:13px\">&nbsp;&nbsp;</span></h2>\r\n\r\n<p><span style=\"color:rgb(119, 119, 119); font-family:opensanssemibold,verdana,geneva,sans-serif; font-size:12px\">Мы работаем только на высококачествоенном оборудованиии самых известных производителей, что позволяет нам отшивать продукцию высокого качества. Именно упор на качество дает нам преимущество перед конкурентами и возможность бороться за лидирующие позиции на рынке женской дизайнерской одежды.</span></p>\r\n\r\n<div class=\"container\">\r\n<div style=\"float: left;\r\nwidth: 75%;min-height: 100%;\">\r\n<div class=\"first\"><a class=\"fancybox\" href=\"/bundles/acmedemo/img/upload/images/IMG_1.JPG\" rel=\"gallery\"><img src=\"/bundles/acmedemo/img/upload/images/IMG_1.JPG\" /> </a></div>\r\n\r\n<div class=\"third\"><a class=\"fancybox\" href=\"/bundles/acmedemo/img/upload/images/IMG_3.JPG\" rel=\"gallery\"><img src=\"/bundles/acmedemo/img/upload/images/IMG_3.JPG\" /> </a></div>\r\n\r\n<div class=\"second\"><a class=\"fancybox\" href=\"/bundles/acmedemo/img/upload/images/IMG_2.JPG\" rel=\"gallery\"><img src=\"/bundles/acmedemo/img/upload/images/IMG_2.JPG\" /> </a></div>\r\n</div>\r\n\r\n<div style=\"float: right;\r\nwidth: 25%;min-height: 100%;\">\r\n<div class=\"fourth\"><a class=\"fancybox\" href=\"/bundles/acmedemo/img/upload/images/IMG_4.JPG\" rel=\"gallery\"><img src=\"/bundles/acmedemo/img/upload/images/IMG_4.JPG\" /> </a></div>\r\n\r\n<div class=\"last\"><a class=\"fancybox\" href=\"/bundles/acmedemo/img/upload/images/IMG_5.JPG\" rel=\"gallery\"><img src=\"/bundles/acmedemo/img/upload/images/IMG_5.JPG\" /> </a></div>\r\n</div>\r\n</div>\r\n<script>\r\n    $(document).ready(function() {\r\n        $(\'.fancybox\').fancybox({\r\n        	helpers:  {\r\n		        overlayShow : true, //затемнение основной страницы (фон) true/false\r\n				overlayOpacity : 0.8, //прозрачность фона\r\n				overlayColor : \'#777\', //цвет фона\r\n		    }\r\n    	});\r\n    });\r\n</script>',NULL,NULL);
/*!40000 ALTER TABLE `moda_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moda_photo`
--

DROP TABLE IF EXISTS `moda_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moda_photo` (
  `collection_id` int(11) DEFAULT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hide` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_126489F4514956FD` (`collection_id`),
  CONSTRAINT `FK_126489F4514956FD` FOREIGN KEY (`collection_id`) REFERENCES `moda_collection` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moda_photo`
--

LOCK TABLES `moda_photo` WRITE;
/*!40000 ALTER TABLE `moda_photo` DISABLE KEYS */;
INSERT INTO `moda_photo` VALUES (11,6,'PhaogxgR.jpeg',NULL,NULL),(11,7,'qdfZLxaY.jpeg',NULL,NULL),(11,8,'iEyAaWCP.jpeg',NULL,NULL);
/*!40000 ALTER TABLE `moda_photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moda_settings`
--

DROP TABLE IF EXISTS `moda_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moda_settings` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `keywordsMain` longtext COLLATE utf8_unicode_ci NOT NULL,
  `descriptionMain` longtext COLLATE utf8_unicode_ci NOT NULL,
  `keywordsCollection` longtext COLLATE utf8_unicode_ci NOT NULL,
  `descriptionCollection` longtext COLLATE utf8_unicode_ci NOT NULL,
  `keywordsAbout` longtext COLLATE utf8_unicode_ci NOT NULL,
  `descriptionA` longtext COLLATE utf8_unicode_ci NOT NULL,
  `keywordsNews` longtext COLLATE utf8_unicode_ci NOT NULL,
  `descriptionNews` longtext COLLATE utf8_unicode_ci NOT NULL,
  `keywordsContacts` longtext COLLATE utf8_unicode_ci NOT NULL,
  `descriptionContacts` longtext COLLATE utf8_unicode_ci NOT NULL,
  `textAbout` longtext COLLATE utf8_unicode_ci NOT NULL,
  `textContactsL` longtext COLLATE utf8_unicode_ci NOT NULL,
  `textContactsR` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moda_settings`
--

LOCK TABLES `moda_settings` WRITE;
/*!40000 ALTER TABLE `moda_settings` DISABLE KEYS */;
INSERT INTO `moda_settings` VALUES (1,'женская дизайнерская одежда LERAD Минск купить платья Москва стиль мода Лерад','Женская дизайнерская одежда. Коллекция платьев Весна/Лето 2014 от LERAD','коллекция мода весна лето одежда','Первая коллекция бренда LERAD сезона Весна/Лето 2014','мода информация одежда платья','Информация о компании LERAD','мода новости коллекция','Новости от  LERAD','LERAD контакты адрес телефон','Контактная информация LERAD','<p style=\"text-align:justify\">Мы делаем не просто одежду, мы делаем произведения искусства, которые приносят радость своим владельцам. Каждый раз одевая наше платье или юбку, вы будете ощущать себя кокеткой, романтичной, роскошной и жизнеутверждающей девушкой.&nbsp;Наш бренд предлагает всю палитру стиля &quot;Кэжуал&quot;, главными чертами которого является практичность, удобство, простота силуэтов, непринуждённость сочетаний, а также многослойность. Откровенные модели соседствуют с объёмными и скрытными.</p>\r\n\r\n<p style=\"text-align:justify\">Мы шьём дизайнерскую одежду из натуральных тканей высокого качества. Шёлковые ткани в вашем гардеробе - это не только красиво, но и изысканно. Струящиеся крепдешины, полупрозрачные шифоны, воздушная органза и шёлковый стрейч-атлас - платья из этих тканей сделают ваш летний гардероб ярким, эффектным и неповторимым.</p>\r\n\r\n<p style=\"text-align:center\">В нашей коллекции вы сможете найти платье на любой вкус.</p>','<div class=\"title\" style=\"border: 0px none; padding: 0px; margin: 0px; outline: none; color: rgb(119, 119, 119); font-family: OpenSansSemibold, Verdana, Geneva, sans-serif; font-size: 12px; line-height: 17px; text-align: center;\">\r\n<h2>Частное предприятие &quot;Лерад&quot;</h2>\r\n\r\n<hr />\r\n<p>Главный офис:</p>\r\n\r\n<p>Беларусь 220099, г. Минск</p>\r\n\r\n<p>ул. Брестская 34, к. 35</p>\r\n\r\n<p>регистрационный номер 192297147</p>\r\n\r\n<p>ОКПО 38182305500</p>\r\n</div>\r\n\r\n<div class=\"content ofices-list\" style=\"border: 0px none; padding: 0px; margin: 0px; outline: none; font-size: 12px; line-height: 17px; text-align: center;\">\r\n<div class=\"content ofices-list\" style=\"border: 0px none; padding: 0px; margin: 0px; outline: none;\">\r\n<p><span style=\"font-size:12px\">e-mail: info@lerad-wear.com</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>директор - Яковицкий М.М.</p>\r\n\r\n<p><span style=\"font-size:12px\">тел.: + 375.33.6004236</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Представитель в Российской Федерации:</p>\r\n\r\n<p>Яковицкая С.В.</p>\r\n\r\n<p>тел.: + 7.915.3281209</p>\r\n\r\n<p>тел.: + 7.985.4170924</p>\r\n</div>\r\n</div>','<h2 style=\"text-align:center\">Информация</h2>\r\n\r\n<hr />\r\n<p style=\"text-align:center\">Приглашаем к сотрудничеству магазины женской одежды.</p>\r\n\r\n<p style=\"text-align:center\">Будем рады обсудить варианты по размещению нашей продукции в ваших магазинах.</p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\">Для клиентов:</p>\r\n\r\n<p style=\"text-align:center\">Вы всегда можете связаться с нами и сделать предзаказ интересующего вас платья.</p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\">С уважением,</p>\r\n\r\n<p style=\"text-align:center\">ЧП &nbsp;&quot;Лерад&quot;</p>');
/*!40000 ALTER TABLE `moda_settings` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-06-01  0:00:12
